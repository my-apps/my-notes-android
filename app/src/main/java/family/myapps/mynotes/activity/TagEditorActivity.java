package family.myapps.mynotes.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import family.myapps.mynotes.MyNotesApplication;
import family.myapps.mynotes.R;
import family.myapps.mynotes.adapter.SimpleAdapter;
import family.myapps.mynotes.adapter.TagEditorViewHolder;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.viewmodel.OneTimeObserver;
import family.myapps.mynotes.viewmodel.SpecificViewModelProviderFactory;
import family.myapps.mynotes.viewmodel.TagEditorViewModel;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagEditorActivity extends BaseActivity {

    private SimpleAdapter<Tag> mTagEditorAdapter;
    private TagEditorViewModel mViewModel;

    @Inject
    TagEditorViewHolder.Factory mTagEditorViewHolderFactory;
    @Inject
    SpecificViewModelProviderFactory<TagEditorViewModel> mViewModelProviderFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyNotesApplication.getAppComponent().inject(this);
        setContentView(R.layout.activity_tag_selector);

        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(TagEditorViewModel.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        mTagEditorAdapter = new SimpleAdapter<>(mTagEditorViewHolderFactory);
        RecyclerView list = (RecyclerView) findViewById(R.id.tag_list);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        list.setAdapter(mTagEditorAdapter);
EditText addTagText = (EditText) findViewById(R.id.add_tag_text); addTagText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                createTag(textView.getText().toString());
                textView.setText(null);
                textView.clearFocus();
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.getTags().observe(this, mTagListObserver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTagEditorAdapter.close();
    }

    private void createTag(String name) {
        LiveData<Tag> newTag = mViewModel.createTag(name);
        newTag.observe(this, new OneTimeObserver<Tag>(newTag) {
            @Override
            public void once(@Nullable Tag tag) {
                if (tag == null) {
                    Toast.makeText(TagEditorActivity.this, getString(R.string.duplicate_name_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private final Observer<List<Tag>> mTagListObserver = new Observer<List<Tag>>() {
        @Override
        public void onChanged(@Nullable List<Tag> tags) {
            mTagEditorAdapter.setItems(tags);
        }
    };
}
