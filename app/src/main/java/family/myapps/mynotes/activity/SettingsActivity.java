package family.myapps.mynotes.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.greysonparrelli.permiso.PermisoActivity;

import family.myapps.mynotes.fragment.SettingsFragment;

public class SettingsActivity extends PermisoActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
