package family.myapps.mynotes.activity;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import family.myapps.mynotes.adapter.TagEditorViewHolder;
import family.myapps.mynotes.adapter.TagSelectorAdapter;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class ActivityModule {

    @Provides
    public TagSelectorAdapter.Factory provideTagSelectorAdaptorFactory(@NonNull NoteRepository noteRepository) {
        return new TagSelectorAdapter.Factory(noteRepository);
    }

    @Provides
    public TagEditorViewHolder.Factory TagEditorViewHolderFactory(@NonNull TagRepository tagRepository) {
        return new TagEditorViewHolder.Factory(tagRepository);
    }
}
