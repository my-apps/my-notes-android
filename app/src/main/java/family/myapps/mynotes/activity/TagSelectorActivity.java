package family.myapps.mynotes.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import family.myapps.mynotes.MyNotesApplication;
import family.myapps.mynotes.R;
import family.myapps.mynotes.adapter.TagSelectorAdapter;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.viewmodel.OneTimeObserver;
import family.myapps.mynotes.viewmodel.SpecificViewModelProviderFactory;
import family.myapps.mynotes.viewmodel.TagSelectorViewModel;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagSelectorActivity extends BaseActivity {

    private static final String KEY_NOTE_ID = "note_id";

    private TagSelectorAdapter mTagSelectorAdapter;
    @Inject
    SpecificViewModelProviderFactory<TagSelectorViewModel> mViewModelProviderFactory;
    @Inject
    TagSelectorAdapter.Factory mTagSelectorAdapterFactory;

    TagSelectorViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyNotesApplication.getAppComponent().inject(this);
        setContentView(R.layout.activity_tag_selector);

        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(TagSelectorViewModel.class);
        if (getIntent() == null || !getIntent().hasExtra(KEY_NOTE_ID)) {
            throw new IllegalStateException("Activity must be provided a Note ID.");
        }
        long noteId = getIntent().getLongExtra(KEY_NOTE_ID, 0);
        mViewModel.init(noteId);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        mTagSelectorAdapter = mTagSelectorAdapterFactory.build(noteId);
        RecyclerView list = (RecyclerView) findViewById(R.id.tag_list);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        list.setAdapter(mTagSelectorAdapter);

        EditText addTagText = (EditText) findViewById(R.id.add_tag_text);
        addTagText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                createTag(textView.getText().toString());
                textView.setText(null);
                textView.clearFocus();
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.getTags().observe(this, mTagObserver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTagSelectorAdapter.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void createTag(String name) {
        final LiveData<Tag> newTag = mViewModel.createAndAddTagToNote(name);
        newTag.observe(this, new OneTimeObserver<Tag>(newTag) {
            @Override
            public void once(@Nullable Tag tag) {
                if (tag == null) {
                    Toast.makeText(TagSelectorActivity.this, getString(R.string.duplicate_name_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static Intent buildIntent(Context context, long noteId) {
        Intent intent = new Intent(context, TagSelectorActivity.class);
        intent.putExtra(KEY_NOTE_ID, noteId);
        return intent;
    }

    private final Observer<List<Tag>> mTagObserver = new Observer<List<Tag>>() {
        @Override
        public void onChanged(@Nullable List<Tag> tags) {
            mTagSelectorAdapter.setItems(tags);
        }
    };
}
