package family.myapps.mynotes.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import family.myapps.mynotes.MyNotesApplication;
import family.myapps.mynotes.R;
import family.myapps.mynotes.adapter.NoteListAdapter;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.adapter.SimpleAdapter;
import family.myapps.mynotes.adapter.TagListViewHolder;
import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;
import family.myapps.mynotes.util.UiUtil;
import family.myapps.mynotes.viewmodel.NoteListViewModel;
import family.myapps.mynotes.viewmodel.OneTimeObserver;
import family.myapps.mynotes.viewmodel.SpecificViewModelProviderFactory;

public class NoteListActivity extends BaseActivity {

    private static final String KEY_TAG = "tag";
    private static final String KEY_SEARCH_MODE_ENABLED = "search_mode_enabled";
    private static final String KEY_QUERY = "query";
    private static final int NUM_COLUMNS = 2;

    @Inject
    NoteRepository mNoteRepository;
    @Inject
    TagRepository mTagRepository;
    @Inject
    SpecificViewModelProviderFactory<NoteListViewModel> mViewModelProviderFactory;
    private NoteListViewModel mViewModel;

    private RecyclerView mNoteList;
    private RecyclerView mTagList;
    private FloatingActionButton mFab;
    private DrawerLayout mDrawerLayout;
    private View mDrawerContainer;
    private MenuItem mSearchMenuItem;
    private View mSearchActionBar;
    private SwipeRefreshLayout mPtrContainer;

    private NoteListAdapter mNoteListAdapter;
    private SimpleAdapter<Tag> mTagListAdapter;

    private Tag mActiveTag;
    private boolean mIsSearchModeEnabled;
    private String mActiveQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyNotesApplication.getAppComponent().inject(this);
        setContentView(R.layout.activity_note_list);
        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(NoteListViewModel.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable menuIcon = VectorDrawableCompat.create(getResources(), R.drawable.ic_menu_white_24px, null);
        getSupportActionBar().setHomeAsUpIndicator(menuIcon);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerContainer = findViewById(R.id.drawer_container);
        toolbar.setNavigationOnClickListener(view -> {
            if (mDrawerLayout.isDrawerOpen(mDrawerContainer)) {
                mDrawerLayout.closeDrawer(mDrawerContainer);
            } else {
                mDrawerLayout.openDrawer(mDrawerContainer);
            }
        });

        mNoteList = (RecyclerView) findViewById(R.id.note_list);
        mNoteList.setLayoutManager(new StaggeredGridLayoutManager(NUM_COLUMNS, StaggeredGridLayoutManager.VERTICAL));

        mNoteListAdapter = new NoteListAdapter(this, mTagRepository);
        mNoteListAdapter.setCallback(mOnNoteClickedListener);
        mNoteList.setAdapter(mNoteListAdapter);

        mTagList = (RecyclerView) findViewById(R.id.tag_list);
        mTagList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mTagListAdapter = new SimpleAdapter<>(new TagListViewHolder.Factory());
        mTagListAdapter.setCallback(mOnTagClickedListener);
        mTagList.setAdapter(mTagListAdapter);

        findViewById(R.id.all_notes_btn).setOnClickListener(view -> onAllNotesButtonClicked());
        findViewById(R.id.settings_btn).setOnClickListener(view -> onSettingsButtonClicked());

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener((View view) -> {
            LiveData<Note> liveNote = mNoteRepository.newNote();
            liveNote.observe(NoteListActivity.this, new OneTimeObserver<Note>(liveNote) {
                @Override
                public void once(@Nullable Note note) {
                    Intent intent = NoteActivity.buildIntent(NoteListActivity.this, note);
                    startActivity(intent);
                }
            });
        });

        findViewById(R.id.edit_btn).setOnClickListener(view -> onEditTagsButtonClicked());

        mPtrContainer = (SwipeRefreshLayout) findViewById(R.id.ptr_container);
        mPtrContainer.setOnRefreshListener(() -> {
            mViewModel.rebuildFromDisk();
            mPtrContainer.setRefreshing(false);
        });

        // Don't bother doing this stuff after rotation
        if (savedInstanceState == null) {
            mPtrContainer.setRefreshing(true);
            mViewModel.rebuildFromDisk();
            filterByTag(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_list, menu);
        mSearchMenuItem = menu.findItem(R.id.search);

        mSearchMenuItem.setOnMenuItemClickListener(menuItem -> {
            setSearchModeEnabled(true);
            return true;
        });

        // Set icon manually here for API < 21
        VectorDrawableCompat searchIcon = VectorDrawableCompat.create(
                getResources(),
                R.drawable.ic_search_white_24px,
                null);
        mSearchMenuItem.setIcon(searchIcon);

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.getTags().observe(this, mTagListObserver);
        mViewModel.getPinnedNotes().observe(this, mPinnedNoteListObserver);
        mViewModel.getUnpinnedNotes().observe(this, mUnpinnedNoteListObserver);

        filterByTag(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTagListAdapter.close();
        mNoteListAdapter.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_TAG, mActiveTag);
        outState.putBoolean(KEY_SEARCH_MODE_ENABLED, mIsSearchModeEnabled);
        outState.putString(KEY_QUERY, mActiveQuery);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mActiveTag = savedInstanceState.getParcelable(KEY_TAG);
        filterByTag(mActiveTag);
        mIsSearchModeEnabled = savedInstanceState.getBoolean(KEY_SEARCH_MODE_ENABLED, false);
        if (mIsSearchModeEnabled) {
            setSearchModeEnabled(true);
            mActiveQuery = savedInstanceState.getString(KEY_QUERY, null);
            EditText searchInput = (EditText) mSearchActionBar.findViewById(R.id.search_input);
            searchInput.setText(mActiveQuery);
            searchInput.setSelection(mActiveQuery.length());
            filterByQuery(mActiveQuery);
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsSearchModeEnabled) {
            setSearchModeEnabled(false);
        } else {
            super.onBackPressed();
        }
    }

    private void setSearchModeEnabled(boolean enabled) {
        mIsSearchModeEnabled = enabled;
        if (enabled) {
            if (mSearchActionBar == null) {
                mSearchActionBar = getLayoutInflater().inflate(R.layout.action_bar_search, null);
            }
            getSupportActionBar().setCustomView(mSearchActionBar);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            if (mSearchMenuItem != null) {
                mSearchMenuItem.setVisible(false);
            }
            mSearchActionBar.findViewById(R.id.close_search_btn).setOnClickListener(view -> setSearchModeEnabled(false));

            EditText searchInput = (EditText) mSearchActionBar.findViewById(R.id.search_input);
            searchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                @Override
                public void afterTextChanged(Editable editable) { }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    filterByQuery(charSequence.toString().trim());
                }
            });
            searchInput.requestFocus();
            UiUtil.showKeyboard(this, searchInput);
        } else {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            filterByQuery(null);
            if (mSearchMenuItem != null) {
                mSearchMenuItem.setVisible(true);
            }
            if (mSearchActionBar != null) {
                EditText searchInput = (EditText) mSearchActionBar.findViewById(R.id.search_input);
                searchInput.setText(null);
                searchInput.clearFocus();
            }
        }
    }

    private void filterByTag(Tag tag) {
        mActiveTag = tag;
        if (tag != null) {
            mViewModel.filterNotesByTagId(tag.getId());
            getSupportActionBar().setTitle(tag.getName());
            ColorDrawable backgroundColor = new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimaryAlt));
            getSupportActionBar().setBackgroundDrawable(backgroundColor);
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDarkAlt));
            }
        } else {
            mViewModel.filterNotesByQuery(null);
            getSupportActionBar().setTitle(getString(R.string.all_notes_title));
            ColorDrawable backgroundColor = new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary));
            getSupportActionBar().setBackgroundDrawable(backgroundColor);
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }
    }

    private void filterByQuery(String query) {
        mActiveQuery = query;
        if (query != null && query.length() > 0) {
            mViewModel.filterNotesByQuery(query);
        } else {
            filterByTag(mActiveTag);
        }
    }

    private void onEditTagsButtonClicked() {
        mDrawerLayout.closeDrawer(mDrawerContainer);
        startActivity(new Intent(this, TagEditorActivity.class));
    }

    private void onAllNotesButtonClicked() {
        mDrawerLayout.closeDrawer(mDrawerContainer);
        filterByTag(null);
    }

    private void onSettingsButtonClicked() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private final Observer<List<Tag>> mTagListObserver = new Observer<List<Tag>>() {
        @Override
        public void onChanged(@Nullable List<Tag> tagList) {
            mTagListAdapter.setItems(tagList);
        }
    };

    private final Observer<List<Note>> mPinnedNoteListObserver = new Observer<List<Note>>() {
        @Override
        public void onChanged(@Nullable List<Note> notes) {
            mNoteListAdapter.setPinnedNotes(notes);

            // If a refresh is happening, that means the refresh just finished now
            if (!mPtrContainer.isRefreshing()) {
                return;
            }
            if (mActiveQuery != null) {
                filterByQuery(mActiveQuery);
            } else {
                filterByTag(mActiveTag);
            }
            mPtrContainer.setRefreshing(false);
        }
    };

    private final Observer<List<Note>> mUnpinnedNoteListObserver = new Observer<List<Note>>() {
        @Override
        public void onChanged(@Nullable List<Note> notes) {
            mNoteListAdapter.setUnpinnedNotes(notes);

            // If a refresh is happening, that means the refresh just finished now
            if (!mPtrContainer.isRefreshing()) {
                return;
            }
            if (mActiveQuery != null) {
                filterByQuery(mActiveQuery);
            } else {
                filterByTag(mActiveTag);
            }
            mPtrContainer.setRefreshing(false);
        }
    };

    private SimpleAdapter.Callback<Note> mOnNoteClickedListener = new SimpleAdapter.Callback<Note>() {
        @Override
        public void onItemClicked(Note note) {
            startActivity(NoteActivity.buildIntent(NoteListActivity.this, note));
        }

        @Override
        public void notifyDataSetChanged() {
            mNoteListAdapter.notifyDataSetChanged();
        }
    };

    private SimpleAdapter.Callback<Tag> mOnTagClickedListener = new SimpleAdapter.Callback<Tag>() {
        @Override
        public void onItemClicked(Tag tag) {
            mDrawerLayout.closeDrawer(mDrawerContainer);
            filterByTag(tag);
        }

        @Override
        public void notifyDataSetChanged() {
            mNoteListAdapter.notifyDataSetChanged();
        }
    };
}
