package family.myapps.mynotes.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import family.myapps.mynotes.MyNotesApplication;
import family.myapps.mynotes.R;
import family.myapps.mynotes.adapter.NoteTagPillViewHolder;
import family.myapps.mynotes.adapter.SimpleAdapter;
import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.ui.MyNotesEditText;
import family.myapps.mynotes.viewmodel.NoteViewModel;
import family.myapps.mynotes.viewmodel.SpecificViewModelProviderFactory;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteActivity extends BaseActivity {

    private static final String KEY_NOTE_ID = "note";

    private SimpleAdapter<Tag> mNoteTagListAdapter;

    private EditText mTitle;
    private MyNotesEditText mBody;
    private RecyclerView mTagList;
    private View mAddTagButton;

    private ActionBarMode mActionBarMode = ActionBarMode.NORMAL;
    private String mActiveUrl;

    @Inject
    SpecificViewModelProviderFactory<NoteViewModel> mViewModelProviderFactory;

    private NoteViewModel mViewModel;
    private boolean mIsNotePinned;
    private boolean mFirstNoteUpdate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyNotesApplication.getAppComponent().inject(this);
        setContentView(R.layout.activity_note);

        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(NoteViewModel.class);
        Intent intent = getIntent();
        if (intent == null) {
            throw new IllegalStateException("Activity must be provided a Note object.");
        } else if (Intent.ACTION_SEND.equals(intent.getAction())) {
            String shareText = intent.getStringExtra(Intent.EXTRA_TEXT);
            mViewModel.init(shareText);
        } else if (intent.hasExtra(KEY_NOTE_ID)) {
            long noteId = getIntent().getLongExtra(KEY_NOTE_ID, 0);
            mViewModel.init(noteId);
        } else {
            throw new IllegalStateException("Activity must be provided a Note ID.");
        }

        mTitle = (EditText) findViewById(R.id.title);
        mBody = (MyNotesEditText) findViewById(R.id.body);
        mTagList = (RecyclerView) findViewById(R.id.tag_list);
        mAddTagButton = findViewById(R.id.add_tag_btn);

        mNoteTagListAdapter = new SimpleAdapter<>(new NoteTagPillViewHolder.Factory());
        mTagList.setAdapter(mNoteTagListAdapter);
        mTagList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mAddTagButton.setOnClickListener(view -> onAddTagButtonClick());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        switch (mActionBarMode) {
            case NORMAL:
                getMenuInflater().inflate(R.menu.menu_note, menu);
                MenuItem pin = menu.findItem(R.id.pin);
                if (mIsNotePinned) {
                    pin.setIcon(R.drawable.ic_star_white_24px);
                } else {
                    pin.setIcon(R.drawable.ic_star_border_white_24px);
                }
                return true;
            case LINK:
                getMenuInflater().inflate(R.menu.menu_note_link, menu);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.pin:
                mViewModel.toggleNotePinnedStatus();
                return true;
            case R.id.delete:
                mViewModel.deleteNote();
                finish();
                return true;
            case R.id.open_link:
                openUrlInBrowser(mActiveUrl);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mFirstNoteUpdate = true;
        mViewModel.getNote().observe(this, mNoteObserver);
        mViewModel.getTags().observe(this, mTagObserver);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mTitle.removeTextChangedListener(mTitleTextWatcher);
        mBody.setOnTextEventListner(null);
        mNoteTagListAdapter.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // If the note is empty, delete it when we leave
        if (mTitle.getText().length() == 0 && mBody.getText().length() == 0) {
            mViewModel.deleteNote();
        }
        super.onBackPressed();
    }

    public static Intent buildIntent(Context context, Note note) {
        Intent intent = new Intent(context, NoteActivity.class);
        intent.putExtra(KEY_NOTE_ID, note.getId());
        return intent;
    }

    private void onAddTagButtonClick() {
        startActivity(TagSelectorActivity.buildIntent(this, mViewModel.getNoteId()));
    }

    private void setActiveUrl(String url) {
        if (url != null) {
            mActiveUrl = url;
            mActionBarMode = ActionBarMode.LINK;
            supportInvalidateOptionsMenu();
        } else if (mActionBarMode != ActionBarMode.NORMAL) {
            mActionBarMode = ActionBarMode.NORMAL;
            supportInvalidateOptionsMenu();
        }
    }

    private void openUrlInBrowser(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private final Observer<Note> mNoteObserver = new Observer<Note>() {
        @Override
        public void onChanged(@Nullable Note note) {
            if (note == null) {
                // TODO: What should we do? Back out?
                return;
            }
            if (mFirstNoteUpdate) {
                // Set text before adding listener
                mTitle.setText(note.getTitle());
                mBody.setText(note.getBody());

                // Add text change listeners
                mTitle.addTextChangedListener(mTitleTextWatcher);
                mBody.setOnTextEventListner(mOnBodyTextEventListener);

                // Only request focus on the title if there's not text content
                if (mTitle.getText().length() == 0 && mBody.getText().length() == 0) {
                    mTitle.requestFocus();
                } else {
                    mTitle.clearFocus();
                }
                mFirstNoteUpdate = false;
            }

            if (mIsNotePinned != note.isPinned()) {
                mIsNotePinned = note.isPinned();
                supportInvalidateOptionsMenu();
            }
        }
    };

    private final Observer<List<Tag>> mTagObserver = new Observer<List<Tag>>() {
        @Override
        public void onChanged(@Nullable List<Tag> tags) {
            mNoteTagListAdapter.setItems(tags);
        }
    };

    private final TextWatcher mTitleTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void afterTextChanged(Editable editable) {
            saveCurrentNoteContents();
        }
    };

    private final MyNotesEditText.OnTextEventListener mOnBodyTextEventListener = new MyNotesEditText.OnTextEventListener() {
        @Override
        public void onTextChanged() {
            saveCurrentNoteContents();
        }

        @Override
        public void onLinkHighlighted(String url) {
            setActiveUrl(url);
        }
    };

    private void saveCurrentNoteContents() {
        mViewModel.updateNoteContents(mTitle.getText().toString(), mBody.getText().toString());
    }

    private enum ActionBarMode {
        NORMAL,
        LINK
    }
}
