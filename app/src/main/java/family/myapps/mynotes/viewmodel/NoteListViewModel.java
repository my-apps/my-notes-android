package family.myapps.mynotes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteListViewModel extends ViewModel {

    private final NoteRepository mNoteRepository;
    private final TagRepository mTagRepository;
    private MutableLiveData<List<Note>> mPinnedNotes;
    private MutableLiveData<List<Note>> mUnpinnedNotes;

    NoteListViewModel(@NonNull NoteRepository noteRepository, @NonNull TagRepository tagRepository) {
        mNoteRepository = noteRepository;
        mTagRepository = tagRepository;
    }

    public MutableLiveData<List<Note>> getPinnedNotes() {
        if (mPinnedNotes == null) {
           mPinnedNotes = mNoteRepository.getLivePinnedNotes();
        }
        return mPinnedNotes;
    }

    public MutableLiveData<List<Note>> getUnpinnedNotes() {
        if (mUnpinnedNotes == null) {
            mUnpinnedNotes = mNoteRepository.getLiveUnpinnedNotes();
        }
        return mUnpinnedNotes;
    }

    public LiveData<List<Tag>> getTags() {
        return mTagRepository.getTagsLive();
    }

    public void filterNotesByQuery(String query) {
        if (query != null) {
            mPinnedNotes.postValue(new ArrayList<>());
            mNoteRepository.filterByQuery(mUnpinnedNotes, query);
        } else {
            mNoteRepository.fillPinnedNotes(mPinnedNotes);
            mNoteRepository.fillUnpinnedNotes(mUnpinnedNotes);
        }
    }

    public void filterNotesByTagId(long tagId) {
        // TODO: Split by pinned status? This still shows all relevant notes, but at one block.
        mPinnedNotes.postValue(new ArrayList<>());
        mNoteRepository.filterByTagId(mUnpinnedNotes, tagId);
    }

    public void rebuildFromDisk() {
        mNoteRepository.rebuildFromDisk(getPinnedNotes(), getUnpinnedNotes());
    }
}
