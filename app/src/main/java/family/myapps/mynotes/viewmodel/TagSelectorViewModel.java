package family.myapps.mynotes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagSelectorViewModel extends ViewModel {

    private TagRepository mTagRepository;

    private long mNoteId;

    TagSelectorViewModel(@NonNull TagRepository tagRepository) {
        mTagRepository = tagRepository;
    }

    public void init(long noteId) {
        mNoteId = noteId;
    }

    public long getNoteId() {
        return mNoteId;
    }

    public LiveData<List<Tag>> getTags() {
        return mTagRepository.getTagsLive();
    }

    public LiveData<Tag> createAndAddTagToNote(String name) {
        return mTagRepository.createAndAddTagToNote(name, mNoteId);
    }
}
