package family.myapps.mynotes.viewmodel;

import android.support.annotation.NonNull;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class ViewModelModule {

    @Provides
    public NoteViewModel provideNoteViewModel(@NonNull NoteRepository noteRepository, @NonNull TagRepository tagRepository) {
        return new NoteViewModel(noteRepository, tagRepository);
    }

    @Provides
    public SpecificViewModelProviderFactory<NoteViewModel> provideNoteViewModelProviderFactory(@NonNull Provider<NoteViewModel> noteViewModel) {
        return new SpecificViewModelProviderFactory<>(noteViewModel);
    }

    @Provides
    public TagSelectorViewModel provideTagSelectorViewModel(@NonNull TagRepository tagRepository) {
        return new TagSelectorViewModel(tagRepository);
    }

    @Provides
    public SpecificViewModelProviderFactory<TagSelectorViewModel> provideTagSelectorModelProviderFactory(@NonNull Provider<TagSelectorViewModel> tagSelecorViewModel) {
        return new SpecificViewModelProviderFactory<>(tagSelecorViewModel);
    }

    @Provides
    public TagEditorViewModel provideTagEditorViewModel(@NonNull TagRepository tagRepository) {
        return new TagEditorViewModel(tagRepository);
    }

    @Provides
    public SpecificViewModelProviderFactory<TagEditorViewModel> provideTagEditorViewModelProviderFactory(@NonNull Provider<TagEditorViewModel> tagEditorViewModel) {
        return new SpecificViewModelProviderFactory<>(tagEditorViewModel);
    }

    @Provides
    public NoteListViewModel provideNoteListViewModel(@NonNull NoteRepository noteRepository, @NonNull TagRepository tagRepository) {
        return new NoteListViewModel(noteRepository, tagRepository);
    }

    @Provides
    public SpecificViewModelProviderFactory<NoteListViewModel> provideNoteListViewModelProviderFactory(@NonNull Provider<NoteListViewModel> noteListViewModel) {
        return new SpecificViewModelProviderFactory<>(noteListViewModel);
    }
}
