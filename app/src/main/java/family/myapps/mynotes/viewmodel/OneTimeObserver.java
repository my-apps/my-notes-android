package family.myapps.mynotes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public abstract class OneTimeObserver<T> implements Observer<T> {

    private final LiveData<T> mObservable;

    public OneTimeObserver(LiveData<T> observable) {
        mObservable = observable;
    }

    @Override
    public void onChanged(@Nullable T t) {
        once(t);
        mObservable.removeObserver(this);
    }

    public abstract void once(@Nullable T t);
}
