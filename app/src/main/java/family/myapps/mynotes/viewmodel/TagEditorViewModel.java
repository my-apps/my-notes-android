package family.myapps.mynotes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagEditorViewModel extends ViewModel {

    private final TagRepository mTagRepository;

    TagEditorViewModel(@NonNull TagRepository tagRepository) {
        mTagRepository = tagRepository;
    }

    public LiveData<List<Tag>> getTags() {
        return mTagRepository.getTagsLive();
    }

    public LiveData<Tag> createTag(String name) {
        return mTagRepository.createTag(name);
    }
}
