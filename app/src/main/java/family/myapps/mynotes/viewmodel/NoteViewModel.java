package family.myapps.mynotes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;


import java.util.List;

import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteViewModel extends ViewModel {

    private final NoteRepository mNoteRepository;
    private final TagRepository mTagRepository;

    private long mNoteId;

    NoteViewModel(@NonNull NoteRepository noteRepository, @NonNull TagRepository tagRepository) {
        mNoteRepository = noteRepository;
        mTagRepository = tagRepository;
    }

    public void init(long id) {
        mNoteId = id;
    }

    public void init(String body) {
        // TODO: Make this update thing cleaner, make make a newNote() method that takes these params?
        Note note = mNoteRepository.newNoteSync();
        mNoteId = note.getId();
        mNoteRepository.updateNoteById(mNoteId, "", body);
    }

    public long getNoteId() {
        return mNoteId;
    }

    public LiveData<Note> getNote() {
        return mNoteRepository.getLiveById(mNoteId);
    }

    public void updateNoteContents(String title, String body) {
        mNoteRepository.updateNoteById(mNoteId, title, body);
    }

    public void toggleNotePinnedStatus() {
        mNoteRepository.togglePinnedStatusById(mNoteId);
    }

    public void deleteNote() {
        mNoteRepository.deleteById(mNoteId);
    }

    public LiveData<List<Tag>> getTags() {
        return mTagRepository.getLiveByNoteId(mNoteId);
    }
}
