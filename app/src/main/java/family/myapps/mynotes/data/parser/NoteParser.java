package family.myapps.mynotes.data.parser;

import family.myapps.mynotes.data.NoteWithTags;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public interface NoteParser {
    boolean shouldParse(String fileContents);
    NoteWithTags parse(String fileContents);
    String write(NoteWithTags noteWithTags);
}
