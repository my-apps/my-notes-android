package family.myapps.mynotes.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import family.myapps.mynotes.adapter.StableIdHolder;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Entity(tableName = "note", indices = {@Index(value = "path", unique = true)})
public class Note implements SqlModel, StableIdHolder {

    private static final String TAG = "Note";
    private static final String PINNED_CHAR = "*";
    private static final String CREATED_TIMESTAMP_PREFIX = "Created ";
    private static final String UPDATED_TIMESTAMP_PREFIX = "Last updated ";
    private static final String TAG_PREFIX = "#";
    private static final char NEWLINE = '\n';
    private static final char SPACE = ' ';
    private static final SimpleDateFormat[] DATE_FORMATS = {
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()),
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
    };

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    private long id;
    private String path;
    private String title;
    private String body;
    private boolean pinned;
    @ColumnInfo(name = "created_timestamp_ms")
    private long createdTimestampMs;
    @ColumnInfo(name = "updated_timestamp_ms")
    private long updatedTimestampMs;
    private boolean synced;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    public long getCreatedTimestampMs() {
        return createdTimestampMs;
    }

    public void setCreatedTimestampMs(long createdTimestampMs) {
        this.createdTimestampMs = createdTimestampMs;
    }

    public long getUpdatedTimestampMs() {
        return updatedTimestampMs;
    }

    public void setUpdatedTimestampMs(long updatedTimestampMs) {
        this.updatedTimestampMs = updatedTimestampMs;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put("_id", getId());
        cv.put("body", getBody());
        cv.put("title", getTitle());
        cv.put("path", getPath());
        cv.put("created_timestamp_ms", getCreatedTimestampMs());
        cv.put("updated_timestamp_ms", getUpdatedTimestampMs());
        return cv;
    }

    @Override
    public long getItemId() {
        return id;
    }

    public static class Builder implements SqlModel.Builder<Note> {

        @Override
        public Note fromCursor(Cursor cursor) {
            Note note = new Note();
            note.setId(cursor.getLong(cursor.getColumnIndex("_id")));
            note.setPath(cursor.getString(cursor.getColumnIndex("path")));
            note.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            note.setBody(cursor.getString(cursor.getColumnIndex("body")));
            note.setPinned(cursor.getInt(cursor.getColumnIndex("pinned")) == 1);
            note.setCreatedTimestampMs(cursor.getLong(cursor.getColumnIndex("created_timestamp_ms")));
            note.setUpdatedTimestampMs(cursor.getLong(cursor.getColumnIndex("updated_timestamp_ms")));
            return note;
        }
    }
}
