package family.myapps.mynotes.data.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.NoteWithTags;
import family.myapps.mynotes.data.Tag;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Deprecated
public class NoteParserV1 implements NoteParser {

    private static final String HEADER_BOUNDARY = "----------";

    @Override
    public boolean shouldParse(String fileContents) {
        return fileContents.trim().startsWith(HEADER_BOUNDARY);
    }

    @Override
    public String write(NoteWithTags noteWithTags) {
        // Don't really have incentive to fill this out...
        return null;
    }

    @Override
    public NoteWithTags parse(String fileContents) {
        Note note = new Note();
        List<Tag> tags = new ArrayList<>();

        Scanner scanner = new Scanner(fileContents);

        // Read header
        String line = scanner.hasNextLine() ? scanner.nextLine() : null;
        if (HEADER_BOUNDARY.equals(line)) {

            // Get all of the json
            StringBuilder json = new StringBuilder();
            while (scanner.hasNextLine() && !(line = scanner.nextLine()).equals(HEADER_BOUNDARY)) {
                json.append(line).append('\n');
            }

            // Parse the json
            try {
                JSONObject object = new JSONObject(json.toString());
                if (object.has("title")) {
                    note.setTitle(object.getString("title"));
                }
                if (object.has("pinned")) {
                    note.setPinned(object.getBoolean("pinned"));
                }
                if (object.has("tags")) {
                    JSONArray tagsJson = object.getJSONArray("tags");
                    for (int i = 0 ; i < tagsJson.length(); i++) {
                        Tag tag  = new Tag();
                        tag.setName(tagsJson.getString(i));
                        tags.add(tag);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Read the body
        StringBuilder body = new StringBuilder();
        while(scanner.hasNextLine()) {
            body.append(scanner.nextLine()).append('\n');
        }

        note.setBody(body.toString());

        return new NoteWithTags(note, tags);
    }
}
