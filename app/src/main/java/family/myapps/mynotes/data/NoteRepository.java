package family.myapps.mynotes.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import family.myapps.mynotes.data.parser.NoteParserV2;

public class NoteRepository {

    private final AppDatabase mDatabase;
    private final FileStorage mFileStorage;
    private final NoteDao mNoteDao;
    private final TagDao mTagDao;
    private final NoteTagDao mNoteTagDao;
    private final Executor mExecutor;

    public NoteRepository(
            @NonNull AppDatabase database,
            @NonNull FileStorage fileStorage,
            @NonNull NoteDao noteDao,
            @NonNull TagDao tagDao,
            @NonNull NoteTagDao noteTagDao,
            @NonNull Executor executor) {
        mDatabase = database;
        mFileStorage = fileStorage;
        mNoteDao = noteDao;
        mTagDao = tagDao;
        mNoteTagDao = noteTagDao;
        mExecutor = executor;
    }

    public void rebuildFromDisk(@NonNull MutableLiveData<List<Note>> mPinnedNotes, @NonNull MutableLiveData<List<Note>> mUnpinnedNotes) {
        mExecutor.execute(() -> {
            // Start transaction (necessary - otherwise we post tons of updates to our LiveData listeners!)
            mDatabase.beginTransaction();

            // Loop through the provided iterator and add all notes to the DB
            FileStorage.AllDataIterator iter = mFileStorage.getAllData();
            while (iter.hasNext()) {
                NoteWithTags noteWithTags = iter.next();
                Note foundNote = mNoteDao.getByPath(noteWithTags.note.getPath());
                long noteId;
                if (foundNote != null) {
                    noteId = foundNote.getId();

                    // Update the found entry with the new data parsed from disk
                    noteWithTags.note.setId(noteId);
                    mNoteDao.update(noteWithTags.note);
                } else {
                    noteId = mNoteDao.insert(noteWithTags.note);
                }

                // Mark the note as 'synced'
                mNoteDao.markSynced(noteId);

                // Insert all of the relevant tags
                for (Tag tag : noteWithTags.tags) {
                    Tag foundTag = mTagDao.getByName(tag.getName());
                    long tagId = foundTag != null ? foundTag.getId() : mTagDao.insert(tag);

                    if (mNoteTagDao.get(noteId, tagId) == null) {
                        addTagToNoteSync(noteId, tagId, false);
                    }
                    mNoteTagDao.markSynced(noteId, tagId);
                }
            }

            // Delete all unsynced notes (they must have been deleted from disk)
            mNoteDao.deleteAllUnsynced();

            // Delete all unused note-tag pairings (they must no longer exist)
            mNoteTagDao.deleteAllUnsynced();

            // Delete all unused tags
            mTagDao.deleteAllUnused();

            // Mark all notes and note-tags as unsynced
            mNoteDao.clearAllSyncedState();
            mNoteTagDao.clearAllSyncedState();

            // End transaction
            mDatabase.setTransactionSuccessful();
            mDatabase.endTransaction();

            // Fill the provided lists with the new data
            mPinnedNotes.postValue(mNoteDao.getAllPinned());
            mUnpinnedNotes.postValue(mNoteDao.getAllUnpinned());
        });
    }

    // TODO: Try to remove
    public Note newNoteSync() {
        // Get a filename
        String baseName = NoteParserV2.DATE_FORMAT.format(new Date());
        String filePath = mFileStorage.getUniqueFilePath(
                mFileStorage.getRootDir(),
                baseName,
                "md");

        // Write file
        mFileStorage.writeToFile(filePath," ");

        final long currentTimeMs = System.currentTimeMillis();

        Note note = new Note();
        note.setPath(filePath);
        note.setCreatedTimestampMs(currentTimeMs);
        note.setUpdatedTimestampMs(currentTimeMs);
        note.setId(mNoteDao.insert(note));
        return note;
    }

    public LiveData<Note> newNote() {
        MutableLiveData<Note> liveNote = new MutableLiveData<>();
        mExecutor.execute(() -> {
            liveNote.postValue(newNoteSync());
        });
        return liveNote;
    }

    public void fillPinnedNotes(MutableLiveData<List<Note>> notes) {
        mExecutor.execute(() -> notes.postValue(mNoteDao.getAllPinned()));
    }

    public MutableLiveData<List<Note>> getLivePinnedNotes() {
        MutableLiveData<List<Note>> notes = new MutableLiveData<>();
        mExecutor.execute(() -> notes.postValue(mNoteDao.getAllPinned()));
        return notes;
    }

    public void fillUnpinnedNotes(MutableLiveData<List<Note>> notes) {
        mExecutor.execute(() -> notes.postValue(mNoteDao.getAllUnpinned()));
    }

    public MutableLiveData<List<Note>> getLiveUnpinnedNotes() {
        MutableLiveData<List<Note>> notes = new MutableLiveData<>();
        mExecutor.execute(() -> notes.postValue(mNoteDao.getAllUnpinned()));
        return notes;
    }

    public LiveData<Note> getLiveById(long id) {
        return mNoteDao.getLiveById(id);
    }

    // TODO: Try to remove
    public boolean noteContainsTagIdSync(long noteId, long tagId) {
        return mNoteTagDao.get(noteId, tagId) != null;
    }

    public void filterByQuery(MutableLiveData<List<Note>> notes, String query) {
        mExecutor.execute(() -> notes.postValue(mNoteDao.getByQuery(query)));
    }

    public void filterByTagId(MutableLiveData<List<Note>> notes, long tagId) {
        mExecutor.execute(() -> notes.postValue(mNoteDao.getByTagId(tagId)));
    }

    public void updateNoteById(long id, String title, String body) {
        mExecutor.execute(() -> {
            mNoteDao.updateTitleBody(id, title, body, System.currentTimeMillis());
            Note note = mNoteDao.getById(id);
            mFileStorage.writeNoteToDisk(new NoteWithTags(note, mTagDao.getByNoteId(note.getId())));
        });
    }

    public void togglePinnedStatusById(long id) {
        mExecutor.execute(() -> {
            Note note = mNoteDao.getById(id);
            mNoteDao.updateLastUpdatedTimestampMs(note.getId(), System.currentTimeMillis());
            mNoteDao.updatePinned(note.getId(), note.isPinned() ? 0 : 1);
            note = mNoteDao.getById(id);
            mFileStorage.writeNoteToDisk(new NoteWithTags(note, mTagDao.getByNoteId(note.getId())));
        });
    }

    public void addTagToNote(long noteId, long tagId) {
        mExecutor.execute(() -> {
            addTagToNoteSync(noteId, tagId, true);
        });
    }

    private void addTagToNoteSync(long noteId, long tagId, boolean writeToDisk) {
        // Create the new note-tag binding
        NoteTag noteTag = new NoteTag();
        noteTag.setNoteId(noteId);
        noteTag.setTagId(tagId);
        mNoteTagDao.insert(noteTag);

        // Update the note's updated timestamp (since the disk timestamp will be updated)
        mNoteDao.updateLastUpdatedTimestampMs(noteId, System.currentTimeMillis());

        // Update the note's representation on disk
        if (writeToDisk) {
            mFileStorage.writeNoteToDisk(new NoteWithTags(
                    mNoteDao.getById(noteId),
                    mTagDao.getByNoteId(noteId)));
        }
    }

    public void removeTagFromNote(long noteId, long tagId) {
        mExecutor.execute(() -> {
            // TODO: Delete tag if it's the last reference? Might not be necessary, we'll rebuild next open anyway
            mNoteTagDao.delete(noteId, tagId);

            // Update the note's updated timestamp (since the disk timestamp will be updated)
            mNoteDao.updateLastUpdatedTimestampMs(noteId, System.currentTimeMillis());

            // Update the note's representation on disk
            Note note = mNoteDao.getById(noteId);
            mFileStorage.writeNoteToDisk(new NoteWithTags(note, mTagDao.getByNoteId(note.getId())));
        });
    }

    public void deleteById(long id) {
        mExecutor.execute(() -> {
            mFileStorage.deleteFile(mNoteDao.getById(id).getPath());
            mNoteDao.delete(id);
        });
    }
}
