package family.myapps.mynotes.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "note_tag",
        indices = {
                    @Index(value = "note_id"),
                    @Index(value = "tag_id"),
                    @Index(value = {"note_id", "tag_id"}, unique = true)},
        foreignKeys = {
        @ForeignKey(entity = Note.class,
                    parentColumns = "_id",
                    childColumns = "note_id",
                    onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = Tag.class,
                    parentColumns = "_id",
                    childColumns = "tag_id",
                    onDelete = ForeignKey.CASCADE)})
public class NoteTag {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    private long id;
    @ColumnInfo(name = "note_id")
    private long noteId;
    @ColumnInfo(name = "tag_id")
    private long tagId;
    private boolean synced;

    public NoteTag() {}

    @Ignore
    public NoteTag(long noteId, long tagId) {
        this.noteId = noteId;
        this.tagId = tagId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNoteId() {
        return noteId;
    }

    public void setNoteId(long noteId) {
        this.noteId = noteId;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}
