package family.myapps.mynotes.data.parser;

import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.NoteWithTags;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.util.CircularBuffer;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteParserV2 implements NoteParser {

    private static final String PINNED_CHAR = "*";
    private static final String CREATED_TIMESTAMP_PREFIX = "Created ";
    private static final String UPDATED_TIMESTAMP_PREFIX = "Last updated ";
    private static final String TAG_PREFIX = "#";
    private static final char NEWLINE = '\n';
    private static final char SPACE = ' ';
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static final SimpleDateFormat[] DATE_FORMATS = {
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()),
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
    };

    @Override
    public boolean shouldParse(String fileContents) {
        return true;
    }

    @Override
    public String write(NoteWithTags noteWithTags) {
        StringBuilder output = new StringBuilder();

        // Title
        if (noteWithTags.note.isPinned()) {
            output.append(PINNED_CHAR);
        }
        output.append(noteWithTags.note.getTitle()).append(NEWLINE);

        // Leave a blank line after the title
        output.append(NEWLINE);

        // Body
        output.append(noteWithTags.note.getBody()).append(NEWLINE);

        // Leave a blank line after the body
        output.append(NEWLINE);

        // Timestamps
        output.append(CREATED_TIMESTAMP_PREFIX).append(DATE_FORMAT.format(new Date(noteWithTags.note.getCreatedTimestampMs()))).append(NEWLINE);
        output.append(UPDATED_TIMESTAMP_PREFIX).append(DATE_FORMAT.format(new Date(noteWithTags.note.getUpdatedTimestampMs()))).append(NEWLINE);

        // Tags
        for (Tag tag : noteWithTags.tags) {
            output.append(TAG_PREFIX).append(tag.getName()).append(SPACE);
        }

        return output.toString();
    }

    @Override
    public NoteWithTags parse(String fileContents) {
        Scanner scanner = new Scanner(fileContents);

        Note note = new Note();

        // Read title
        if (scanner.hasNextLine()) {
            note.setTitle(scanner.nextLine());
        }

        // Pin the note if the title starts with an asterisk
        if (note.getTitle().startsWith(PINNED_CHAR)) {
            note.setPinned(true);
            note.setTitle(note.getTitle().substring(1).trim());
        }

        // Read the rest in as the body
        StringBuilder body = new StringBuilder();
        CircularBuffer<String> recentLinesBuffer = new CircularBuffer<>(3);
        while (scanner.hasNextLine()) {
            String popped = recentLinesBuffer.addAndGet(scanner.nextLine());

            // We want to make sure not to add the last three lines to the body until we're sure
            // that they're not metadata
            if (popped != null) {
                body.append(popped).append(NEWLINE);
            }
        }

        // Look through the last lines for metadata
        List<String> lastLines = recentLinesBuffer.toList();

        long createdTimestampMs = 0;
        long updatedTimestampMs = 0;
        List<Tag> tags = null;

        if (lastLines.size() == 1) {
            createdTimestampMs = parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(0));
            updatedTimestampMs = parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(0));
            tags = parseTags(recentLinesBuffer.get(0));
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0 && tags == null) {
                body.append(lastLines.get(0)).append(NEWLINE);
            }
        } else if (lastLines.size() == 2) {
            createdTimestampMs = parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(0));
            updatedTimestampMs = parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(0));
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0) {
                body.append(lastLines.get(0)).append(NEWLINE);
            }

            createdTimestampMs = createdTimestampMs <= 0 ? parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(1)) : createdTimestampMs;
            updatedTimestampMs = updatedTimestampMs <= 0 ? parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(1)) : updatedTimestampMs;
            tags = parseTags(recentLinesBuffer.get(1));
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0 && tags == null) {
                body.append(lastLines.get(1)).append(NEWLINE);
            }
        } else if (lastLines.size() == 3) {
            createdTimestampMs = parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(0));
            updatedTimestampMs = parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(0));
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0) {
                body.append(lastLines.get(0)).append(NEWLINE);
            }

            createdTimestampMs = createdTimestampMs <= 0 ? parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(1)) : createdTimestampMs;
            updatedTimestampMs = updatedTimestampMs <= 0 ? parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(1)) : updatedTimestampMs;
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0) {
                body.append(lastLines.get(1)).append(NEWLINE);
            }

            createdTimestampMs = createdTimestampMs <= 0 ? parseTimestamp(CREATED_TIMESTAMP_PREFIX, lastLines.get(2)) : createdTimestampMs;
            updatedTimestampMs = updatedTimestampMs <= 0 ? parseTimestamp(UPDATED_TIMESTAMP_PREFIX, lastLines.get(2)) : updatedTimestampMs;
            tags = parseTags(recentLinesBuffer.get(2));
            if (createdTimestampMs <= 0 && updatedTimestampMs <= 0 && tags == null) {
                body.append(lastLines.get(2)).append(NEWLINE);
            }
        }

        note.setBody(body.toString().trim());
        note.setCreatedTimestampMs(createdTimestampMs);
        note.setUpdatedTimestampMs(updatedTimestampMs);

        tags = tags != null ? tags : new ArrayList<>();
        return new NoteWithTags(note, tags);
    }

    @Nullable
    private static List<Tag> parseTags(String line) {
        if (line == null || !line.startsWith(TAG_PREFIX)) {
            return null;
        }

        // We replace the leading tag prefix to avoid split() returning an empty string as the first element
        String[] tagNames = line.replaceFirst("^" + TAG_PREFIX, "").split(TAG_PREFIX);
        List<Tag> tags = new ArrayList<>(tagNames.length);
        for (String name : tagNames) {
            tags.add(new Tag(name.trim()));
        }
        return tags;
    }

    private static long parseTimestamp(String prefix, String line) {
        if (line == null || !line.startsWith(prefix)) {
            return 0;
        }

        String dateString = line.substring(prefix.length());
        Date date = null;
        int position = 0;
        while(date == null && position < DATE_FORMATS.length) {
            try {
                date = DATE_FORMATS[position].parse(dateString);
            } catch (ParseException e) {
                // We just failed, it's ok
            }
            position++;
        }
        if (date != null) {
            return date.getTime();
        }
        return 0;
    }
}
