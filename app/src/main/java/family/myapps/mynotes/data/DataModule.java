package family.myapps.mynotes.data;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import family.myapps.mynotes.data.parser.NoteParserSelector;
import family.myapps.mynotes.data.parser.NoteParserV1;
import family.myapps.mynotes.data.parser.NoteParserV2;

@Module
public class DataModule {

    @Provides
    @Singleton
    public FileStorage provideFileStorage(@NonNull NoteParserSelector noteParserSelector) {
        return new FileStorage(noteParserSelector);
    }

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase(@NonNull Context context) {
        // Note: Only allowing main thread queries for NoteRepository.newNoteSync()
        return Room.databaseBuilder(context, AppDatabase.class, "my-notes.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    public NoteParserSelector provideNoteParserSelector() {
        return new NoteParserSelector(new NoteParserV1(), new NoteParserV2());
    }

    @Provides
    @Singleton
    public NoteDao provideNoteDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.noteDao();
    }

    @Provides
    @Singleton
    public TagDao provideTagDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.tagDao();
    }

    @Provides
    @Singleton
    public NoteTagDao provideNoteTagDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.noteTagDao();
    }

    @Provides
    @Singleton
    public NoteRepository provideNoteRepository(
            @NonNull AppDatabase appDatabase,
            @NonNull FileStorage fileStorage,
            @NonNull NoteDao noteDao,
            @NonNull TagDao tagDao,
            @NonNull NoteTagDao noteTagDao,
            @NonNull Executor executor) {
        return new NoteRepository(appDatabase, fileStorage, noteDao, tagDao, noteTagDao, executor);
    }

    @Provides
    @Singleton
    public TagRepository provideTagRepository(
            @NonNull FileStorage fileStorage,
            @NonNull TagDao tagDao,
            @NonNull NoteDao noteDao,
            @NonNull NoteTagDao noteTagDao,
            @NonNull Executor executor) {
        return new TagRepository(fileStorage, tagDao, noteDao, noteTagDao, executor);
    }
}
