package family.myapps.mynotes.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.Cursor;

import java.util.List;

@Dao
interface TagDao {

    @Insert
    long insert(Tag tag);

    @Query("SELECT * FROM tag ORDER BY name COLLATE nocase")
    Cursor getAll();

    @Query("SELECT * FROM tag ORDER BY name COLLATE nocase")
    LiveData<List<Tag>> getAllLive();

    @Query("SELECT * FROM tag WHERE _id = :id")
    Tag getById(long id);

    @Query("SELECT * FROM tag WHERE name = :name")
    Tag getByName(String name);

    @Query("SELECT T._id, T.name FROM tag AS T, note_tag AS NT " +
           "WHERE T._id = NT.tag_id AND NT.note_id = :noteId")
    List<Tag> getByNoteId(long noteId);

    @Query("SELECT T._id, T.name " +
           "FROM tag AS T, note_tag AS NT " +
           "WHERE T._id = NT.tag_id AND NT.note_id = :noteId")
    LiveData<List<Tag>> getLiveByNoteId(long noteId);

    @Query("UPDATE tag SET name = :name WHERE _id = :tagId")
    void updateName(long tagId, String name);

    @Query("DELETE FROM tag WHERE _id = :tagId")
    void deleteById(long tagId);

    @Query("DELETE FROM tag")
    void deleteAll();

    @Query("DELETE FROM tag WHERE _id NOT IN (SELECT DISTINCT tag_id FROM note_tag)")
    void deleteAllUnused();
}
