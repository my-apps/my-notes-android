package family.myapps.mynotes.data;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public interface SqlModel {

    ContentValues toContentValues();

    interface Builder<T extends SqlModel> {
        T fromCursor(Cursor cursor);
    }
}
