package family.myapps.mynotes.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(
        version = 5,
        exportSchema = false,
        entities = { Note.class, Tag.class, NoteTag.class })
abstract class AppDatabase extends RoomDatabase {

    public abstract NoteDao noteDao();

    public abstract TagDao tagDao();

    public abstract NoteTagDao noteTagDao();
}
