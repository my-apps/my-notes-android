package family.myapps.mynotes.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.Cursor;

@Dao
interface NoteTagDao {

    @Insert
    void insert(NoteTag noteTag);

    @Query("SELECT * FROM note_tag WHERE note_id = :noteId AND tag_id = :tagId")
    NoteTag get(long noteId, long tagId);

    @Query("UPDATE note_tag SET synced = 1 WHERE note_id = :noteId AND tag_id = :tagId")
    void markSynced(long noteId, long tagId);

    @Query("UPDATE note_tag SET synced = 0")
    void clearAllSyncedState();

    @Query("DELETE FROM note_tag WHERE note_id = :noteId AND tag_id = :tagId")
    void delete(long noteId, long tagId);

    @Query("DELETE FROM note_tag WHERE tag_id = :tagId")
    void deleteAllForTag(long tagId);

    @Query("DELETE FROM note_tag")
    void deleteAll();

    @Query("DELETE FROM note_tag WHERE synced = 0")
    void deleteAllUnsynced();
}
