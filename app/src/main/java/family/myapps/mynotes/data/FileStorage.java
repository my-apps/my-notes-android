package family.myapps.mynotes.data;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import family.myapps.mynotes.data.parser.NoteParser;
import family.myapps.mynotes.data.parser.NoteParserSelector;
import family.myapps.mynotes.storage.Preferences;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class FileStorage {

    private static final String TAG = "FileStorage";

    private NoteParserSelector mNoteParserSelector;

    public FileStorage(@NonNull NoteParserSelector noteParserSelector) {
        mNoteParserSelector = noteParserSelector;
    }

    @WorkerThread
    AllDataIterator getAllData() {
        // List all of the files in the note directory
        File rootDir = getRootDir();
        File[] noteFiles = rootDir.listFiles((dir, name) -> {
            if (name == null || name.startsWith(".")) {
                return false;
            }
            return true;
        });

        return new AllDataIterator(noteFiles, mNoteParserSelector);
    }

    // TODO: Use this, lol - I don't think it works at the moment
    void moveNotesToNewLocation(String oldPath, String newPath) {
        File oldDir = new File(oldPath);
        File newDir = new File(newPath);

        try {
            FileUtils.moveDirectory(oldDir, newDir);
        } catch (IOException e) {
            Log.e(TAG, "Failed to move notes to new directory.");
        }
    }

    void writeToFile(String path, String contents) {
        File file = new File(path);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.append(contents);
        } catch (IOException e) {
            Log.e(TAG, "Failed to write to file: " + path);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
                file.setLastModified(System.currentTimeMillis());
            } catch (IOException e) {
                Log.e(TAG, "Failed to close the writer: " + path);
            }
        }
    }

    void writeNoteToDisk(NoteWithTags noteWithTags) {
        writeToFile(noteWithTags.note.getPath(), mNoteParserSelector.getWriter().write(noteWithTags));
    }

    File getRootDir() {
        return new File(Preferences.getInstance().getStoragePath());
    }

    String getUniqueFilePath(@NonNull File directory, @NonNull String baseName, @NonNull String extension) {
        int copy = 0;
        File noteFile;
        do {
            String suffix = copy > 0 ? "_" + copy : "";
            noteFile = new File(directory, baseName + suffix + "." + extension);
            copy++;
        } while (noteFile.exists());
        return noteFile.getAbsolutePath();
    }

    void deleteFile(@NonNull String path) {
        new File(path).delete();
    }

    static class AllDataIterator implements Iterator<NoteWithTags> {

        private int mPosition = 0;
        private final File[] mNoteFiles;
        private final NoteParserSelector mNoteParserSelector;

        AllDataIterator(@NonNull File[] noteFiles, @NonNull NoteParserSelector noteParserSelector) {
            mNoteFiles = noteFiles;
            mNoteParserSelector = noteParserSelector;
        }

        @Override
        public boolean hasNext() {
            return mPosition < mNoteFiles.length;
        }

        @Override
        public NoteWithTags next() {
            File noteFile = mNoteFiles[mPosition];
            String contents = readFile(noteFile);
            NoteParser parser = mNoteParserSelector.getParser(contents);
            if (parser == null) {
                throw new IllegalStateException("Could not find a suitable parser for the note contents.");
            }
            NoteWithTags noteWithTags = parser.parse(contents);
            noteWithTags.note.setPath(noteFile.getAbsolutePath());

            // Default to file metadata if timestamps weren't provided in the note.
            // Note: No created timestamp equivalent for Android's Java :/
            if (noteWithTags.note.getCreatedTimestampMs() <= 0) {
                noteWithTags.note.setCreatedTimestampMs(noteFile.lastModified());
            }
            if (noteWithTags.note.getUpdatedTimestampMs() <= 0) {
                noteWithTags.note.setUpdatedTimestampMs(noteFile.lastModified());
            }
            mPosition++;
            return noteWithTags;
        }

        private String readFile(File file) {
            FileReader reader = null;
            try {
                reader = new FileReader(file);
                char[] buffer = new char[(int) file.length()];
                int numRead = reader.read(buffer);
                return new String(buffer, 0, numRead);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
