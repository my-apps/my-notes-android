package family.myapps.mynotes.data;

import java.util.List;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteWithTags {

    public final Note note;
    public final List<Tag> tags;

    public NoteWithTags(Note note, List<Tag> tags) {
        this.note = note;
        this.tags = tags;
    }
}
