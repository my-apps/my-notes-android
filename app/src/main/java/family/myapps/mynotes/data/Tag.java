package family.myapps.mynotes.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import family.myapps.mynotes.adapter.StableIdHolder;

@Entity(tableName = "tag", indices = {@Index(value = "name", unique = true)})
public class Tag implements SqlModel, StableIdHolder, Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    private long id;
    private String name;

    public Tag() {
        // No-arg constructor is available
    }

    @Ignore
    public Tag(String name) {
        this.name = name;
    }

    @Ignore
    protected Tag(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues(2);
        cv.put("_id", getId());
        cv.put("name", getName());
        return cv;
    }

    @Override
    public long getItemId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    public static class Builder implements SqlModel.Builder<Tag> {

        @Override
        public Tag fromCursor(Cursor cursor) {
            Tag tag = new Tag();
            tag.setId(cursor.getLong(cursor.getColumnIndex("_id")));
            tag.setName(cursor.getString(cursor.getColumnIndex("name")));
            return tag;
        }
    }
}
