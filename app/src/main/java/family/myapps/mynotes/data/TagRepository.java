package family.myapps.mynotes.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.concurrent.Executor;

public class TagRepository {

    private final FileStorage mFileStorage;
    private final TagDao mTagDao;
    private final NoteDao mNoteDao;
    private final NoteTagDao mNoteTagDao;
    private final Executor mExecutor;
    private final Handler mUiHandler;

    public TagRepository(
            @NonNull FileStorage fileStorage,
            @NonNull TagDao tagDao,
            @NonNull NoteDao noteDao,
            @NonNull NoteTagDao noteTagDao,
            @NonNull Executor executor) {
        mFileStorage = fileStorage;
        mTagDao = tagDao;
        mNoteDao = noteDao;
        mNoteTagDao = noteTagDao;
        mExecutor = executor;
        mUiHandler = new Handler(Looper.getMainLooper());
    }

    public LiveData<Tag> createAndAddTagToNote(String name, long noteId) {
        final MutableLiveData<Tag> retTag = new MutableLiveData<>();
        mExecutor.execute(() -> {
            // Make sure the tag doesn't exist yet
            Tag tag = mTagDao.getByName(name);
            if (tag != null) {
                retTag.postValue(null);
                return;
            }

            // Create the tag
            tag = new Tag(name);
            tag.setId(mTagDao.insert(tag));

            // Add the tag to the note
            NoteTag noteTag = new NoteTag(noteId, tag.getId());
            mNoteTagDao.insert(noteTag);

            // Write the note to disk
            mFileStorage.writeNoteToDisk(new NoteWithTags(
                    mNoteDao.getById(noteId),
                    mTagDao.getByNoteId(noteId)));

            // Update the LiveData with the new value
            retTag.postValue(tag);
        });
        return retTag;
    }

    public LiveData<Tag> createTag(String name) {
        final MutableLiveData<Tag> retTag = new MutableLiveData<>();
        mExecutor.execute(() -> {
            // Make sure the tag doesn't exist yet
            Tag tag = mTagDao.getByName(name);
            if (tag != null) {
                retTag.postValue(null);
                return;
            }

            // Create the tag
            tag = new Tag(name);
            tag.setId(mTagDao.insert(tag));
            retTag.postValue(tag);
        });
        return retTag;
    }

    public Tag getTagByIdSync(long id) {
        return mTagDao.getById(id);
    }

    public LiveData<List<Tag>> getTagsLive() {
        return mTagDao.getAllLive();
    }

    public LiveData<List<Tag>> getLiveByNoteId(long noteId) {
        return mTagDao.getLiveByNoteId(noteId);
    }

    public void updateTagById(long id, String newName, @Nullable Callback<Boolean> callback) {
        mExecutor.execute(() -> {
            // Make sure the new name is available
            if (mTagDao.getByName(newName) != null) {
                if (callback != null) {
                    mUiHandler.post(() -> callback.onComplete(false));
                }
                return;
            }

            // Find all notes
            List<Note> notes = mNoteDao.getByTagId(id);

            // Update the tag name
            mTagDao.updateName(id, newName);

            // Write the notes to disk
            for (Note note : notes) {
                mFileStorage.writeNoteToDisk(new NoteWithTags(note, mTagDao.getByNoteId(note.getId())));
            }

            if (callback != null) {
                mUiHandler.post(() -> callback.onComplete(true));
            }
        });
    }

    public void deleteTagById(long id, @Nullable Callback<Void> callback) {
        mExecutor.execute(() -> {
            // Find all notes
            List<Note> notes = mNoteDao.getByTagId(id);

            // Delete the tag (will cascade delete note_tag entries)
            mTagDao.deleteById(id);

            // Update all of the notes
            for (Note note : notes) {
                mFileStorage.writeNoteToDisk(new NoteWithTags(note, mTagDao.getByNoteId(note.getId())));
            }

            if (callback != null) {
                mUiHandler.post(() -> callback.onComplete(null));
            }
        });
    }

    public interface Callback<T> {
        void onComplete(T results);
    }

}
