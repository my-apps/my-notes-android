package family.myapps.mynotes.data;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Dao
interface NoteDao {

    @Insert
    long insert(Note note);

    @Query("SELECT * FROM note WHERE _id = :id")
    Note getById(long id);

    @Query("SELECT * FROM note WHERE _id = :id")
    LiveData<Note> getLiveById(long id);

    @Query("SELECT * FROM note WHERE path = :path")
    Note getByPath(String path);

    @Query("SELECT * FROM note WHERE pinned = 1 ORDER BY updated_timestamp_ms DESC")
    List<Note> getAllPinned();

    @Query("SELECT * FROM note WHERE pinned = 0 ORDER BY updated_timestamp_ms DESC")
    List<Note> getAllUnpinned();

    @Query("SELECT * FROM note WHERE body LIKE '%' || :query || '%' OR title LIKE '%' || :query || '%' ORDER BY updated_timestamp_ms DESC")
    List<Note> getByQuery(String query);

    @Query("SELECT N._id, N.title, N.body, N.path, N.title, N.body, N.pinned, N.created_timestamp_ms, N.updated_timestamp_ms " +
           "FROM note AS N, note_tag AS NT " +
           "WHERE N._id = NT.note_id AND NT.tag_id = :tagId " +
           "ORDER BY N.updated_timestamp_ms DESC")
    List<Note> getByTagId(long tagId);

    @Update
    void update(Note note);

    @Query("UPDATE note SET updated_timestamp_ms = :timestampMs WHERE _id = :noteId")
    void updateLastUpdatedTimestampMs(long noteId, long timestampMs);

    @Query("UPDATE note SET title = :title, body = :body, updated_timestamp_ms = :timestampMs WHERE _id = :noteId")
    void updateTitleBody(long noteId, String title, String body, long timestampMs);

    @Query("UPDATE note SET pinned = :pinned WHERE _id = :noteId")
    void updatePinned(long noteId, int pinned);

    @Query("UPDATE note SET synced = 1 WHERE _id = :noteId")
    void markSynced(long noteId);

    @Query("UPDATE note SET synced = 0")
    void clearAllSyncedState();

    @Query("DELETE FROM note WHERE _id = :id")
    void delete(long id);

    @Query("DELETE FROM note")
    void deleteAll();

    @Query("DELETE FROM note WHERE synced = 0")
    void deleteAllUnsynced();
}
