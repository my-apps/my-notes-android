package family.myapps.mynotes.data.parser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class NoteParserSelector {

    private final NoteParser[] mParsers;

    public NoteParserSelector(@NonNull NoteParser... parsers) {
        if (parsers.length == 0) {
            throw new IllegalArgumentException("No parsers were provided.");
        }
        mParsers = parsers;
    }

    @Nullable
    public NoteParser getParser(String fileContents) {
        for (NoteParser parser : mParsers) {
            if (parser.shouldParse(fileContents)) {
                return parser;
            }
        }
        return null;
    }

    @NonNull
    public NoteParser getWriter() {
        return mParsers[mParsers.length - 1];
    }
}
