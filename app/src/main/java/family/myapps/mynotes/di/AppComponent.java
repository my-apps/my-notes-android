package family.myapps.mynotes.di;

import javax.inject.Singleton;

import dagger.Component;
import family.myapps.mynotes.activity.ActivityModule;
import family.myapps.mynotes.activity.NoteActivity;
import family.myapps.mynotes.activity.NoteListActivity;
import family.myapps.mynotes.activity.TagEditorActivity;
import family.myapps.mynotes.activity.TagSelectorActivity;
import family.myapps.mynotes.data.DataModule;
import family.myapps.mynotes.viewmodel.ViewModelModule;

@Singleton
@Component(modules = { AppModule.class, DataModule.class, ViewModelModule.class, ActivityModule.class })
public interface AppComponent {
    void inject(NoteListActivity activity);
    void inject(NoteActivity activity);
    void inject(TagEditorActivity activity);
    void inject(TagSelectorActivity activity);
}
