package family.myapps.mynotes.di;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import dagger.Module;
import dagger.Provides;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class AppModule {

    private Context mContext;

    public AppModule(@NonNull Context context) {
        mContext = context;
    }

    @Provides
    public Context provideContext() {
        return mContext;
    }

    @Provides
    public Executor provideStorageExecutor() {
        return Executors.newSingleThreadExecutor();
    }
}
