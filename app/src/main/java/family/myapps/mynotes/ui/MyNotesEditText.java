package family.myapps.mynotes.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.AttributeSet;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyNotesEditText extends android.support.v7.widget.AppCompatEditText implements TextWatcher {

    private static final Pattern BULLET_PATTERN = Pattern.compile("^([\\s]*)(-|\\*|\\+)[\\s]+(.*)$");

    private OnTextEventListener mOnTextEventListener;
    private int mCursorPosition;

    {
        setLinksClickable(false);
    }

    public MyNotesEditText(Context context) {
        super(context);
    }

    public MyNotesEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyNotesEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Linkify.addLinks(this, Linkify.ALL);
        addTextChangedListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {
        int previousCursorPosition = mCursorPosition;
        mCursorPosition = getSelectionStart();
        String bodyText = getText().toString();

        // Only for forward cursor movements after a line break
        if (mCursorPosition > previousCursorPosition && mCursorPosition > 0
                && bodyText.charAt(mCursorPosition - 1) == '\n') {

            // Grab the previous line
            int prevLineEnd = mCursorPosition - 1;
            int prevLineStart = bodyText.lastIndexOf("\n", prevLineEnd - 1);
            prevLineStart++; // ++ to account for the linebreak char
            String prevLine = bodyText.substring(prevLineStart, prevLineEnd);

            // Look at the previous line to see if it starts with a bullet
            Matcher matcher = BULLET_PATTERN.matcher(prevLine);
            if (matcher.find()) {
                String leadingWhitespace = matcher.group(1);
                String bulletChar = matcher.group(2);
                boolean isEmptyContents = matcher.group(3).trim().isEmpty();

                // If there's text on the previous line, continue the bullet chain, otherwise clear the line to
                // stop the bullet train
                if (!isEmptyContents) {
                    editable.insert(mCursorPosition, leadingWhitespace + bulletChar + " ");
                } else {
                    editable.replace(prevLineStart, mCursorPosition, "");
                }
            }

        }

        // Save the note contents
        if (mOnTextEventListener != null) {
            mOnTextEventListener.onTextChanged();
        }
        mCursorPosition = getSelectionStart();

        Linkify.addLinks(this, Linkify.ALL);
    }

    @Override
    protected void onSelectionChanged(int start, int end) {
        super.onSelectionChanged(start, end);

        // We don't care about range selections, just cursor relocations
        if (start != end) {
            if (mOnTextEventListener != null) {
                mOnTextEventListener.onLinkHighlighted(null);
            }
            return;
        }

        Editable content = getText();
        if (content == null) {
            if (mOnTextEventListener != null) {
                mOnTextEventListener.onLinkHighlighted(null);
            }
            mCursorPosition = 0;
            return;
        }

        mCursorPosition = start;

        URLSpan[] urlSpans = content.getSpans(start, end, URLSpan.class);
        if (urlSpans.length <= 0) {
            if (mOnTextEventListener != null) {
                mOnTextEventListener.onLinkHighlighted(null);
            }
            return;
        }

        // Since start == end, it's only possible to have one url
        if (mOnTextEventListener != null) {
            mOnTextEventListener.onLinkHighlighted(urlSpans[0].getURL());
        }
    }

    public void setOnTextEventListner(OnTextEventListener listener) {
        mOnTextEventListener = listener;
    }

    public interface OnTextEventListener {
        void onTextChanged();
        void onLinkHighlighted(String url);
    }
}
