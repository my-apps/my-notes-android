package family.myapps.mynotes.util;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class CircularBuffer<E> {

    private final E[] mBuffer;
    private int mCurrentIndex;
    private int mLength;

    public CircularBuffer(int size) {
        // noinspection unchecked
        mBuffer = (E[]) new Object[size];
        clear();
    }

    public void add(E item) {
        mBuffer[mCurrentIndex] = item;
        mCurrentIndex = getNextIndex();
        mLength = Math.min(mLength + 1, mBuffer.length);
    }

    /**
     * @return The oldest item, if and only if it had to be removed to make room for the new item.
     */
    @Nullable
    public E addAndGet(E item) {
        E popped = null;
        if (mLength == mBuffer.length) {
            popped = mBuffer[mCurrentIndex];
        }
        mBuffer[mCurrentIndex] = item;
        mCurrentIndex = getNextIndex();
        mLength = Math.min(mLength + 1, mBuffer.length);
        return popped;
    }

    public E get(int position) {
        return mBuffer[(mCurrentIndex + position) % mBuffer.length];
    }

    public int size() {
        return mLength;
    }

    public int capacity() {
        return mBuffer.length;
    }

    public List<E> toList() {
        List<E> list = new ArrayList<>();

        // If we've filled the buffer, keep the order while making it a list
        if (mLength == mBuffer.length) {
            for (int i = 0; i < mLength; i++) {
                int position = (mCurrentIndex + i) % mBuffer.length;
                list.add(mBuffer[position]);
            }
            return list;
        }

        // If we didn't fill it, we can just straight add the items to the list in order
        list.addAll(Arrays.asList(mBuffer).subList(0, mCurrentIndex));
        return list;
    }

    public void clear() {
        for (int i = 0; i < mBuffer.length; i++) {
            mBuffer[i] = null;
        }
        mCurrentIndex = 0;
        mLength = 0;
    }

    private int getNextIndex() {
        return (mCurrentIndex + 1) % mBuffer.length;
    }
}
/*
_ _ _

1 _ _
1 2 _
1 2 3
4 2 3
4 5 3

*/
