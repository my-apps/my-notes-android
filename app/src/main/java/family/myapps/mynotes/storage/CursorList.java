package family.myapps.mynotes.storage;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import family.myapps.mynotes.data.SqlModel;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class CursorList<T extends SqlModel> implements List<T> {

    private final Cursor mCursor;
    private final SqlModel.Builder<T> mModelBuilder;

    public CursorList(Cursor cursor, SqlModel.Builder<T> modelBuilder) {
        mCursor = cursor;
        mCursor.moveToFirst();
        mModelBuilder = modelBuilder;
    }

    @Override
    public int size() {
        return mCursor.getCount();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return mCursor.getCount() > 0 && !mCursor.isLast();
            }

            @Override
            public T next() {
                T model = mModelBuilder.fromCursor(mCursor);
                mCursor.moveToNext();
                return model;
            }
        };
    }

    @NonNull
    @Override
    public Object[] toArray() {
        Object[] out = new Object[size()];
        for (int i = 0; i < mCursor.getCount(); i++) {
            mCursor.moveToPosition(i);
            out[i] = mModelBuilder.fromCursor(mCursor);
        }
        return out;
    }

    @Override
    public boolean add(T o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(@NonNull Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int i, @NonNull Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public T get(int i) {
        mCursor.moveToPosition(i);
        return mModelBuilder.fromCursor(mCursor);
    }

    @Override
    public T set(int i, T o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int i, T o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator(int i) {
        throw new UnsupportedOperationException();
    }

    @NonNull
    @Override
    public List<T> subList(int i, int i1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NonNull Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NonNull Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(@NonNull Collection collection) {
        throw new UnsupportedOperationException();
    }

    @NonNull
    @Override
    public T[] toArray(@NonNull Object[] objects) {
        throw new UnsupportedOperationException();
    }

    public void close() {
        mCursor.close();
    }
}
