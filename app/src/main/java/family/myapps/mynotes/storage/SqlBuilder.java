package family.myapps.mynotes.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

/**
 * A simple query builder to make generating and running SQL statements easier and prettier. This is NOT designed to
 * cover the creation of every possible query. It's goal is instead to streamline more common queries.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class SqlBuilder {

    /**
     * A simple query builder to make generating and running SQL statements easier and prettier.
     */
    private List<Object> mQueryParts;

    public SqlBuilder() {
        mQueryParts = new LinkedList<>();
    }

    /**
     * Begin a SQL SELECT statement.
     *
     * EXAMPLE:
     *
     * Cursor cursor = new SqlBuilder
     *      .select("field1", "field2")
     *      .from("table")
     *      .where("field1").equalTo("someValue")
     *      .and("field2").greaterThan("5")
     *      .query(db);
     *
     * @param fields
     *      The fields you want to select. This is your projection. If null, this defaults to "*".
     * @return
     *      A selection builder.
     */
    public Select select(String... fields) {
        Select select = new Select(fields);
        mQueryParts.add(select);
        return select;
    }

    /**
     * Begin a SQL UPDATE statement.
     *
     * EXAMPLE:
     *
     * int numAffected = new SqlBuilder
     *      .update("table")
     *      .value("field1", "newValue")
     *      .where("id").equalTo("1")
     *      .execute(db);
     *
     * @param tableName
     *      The table you'd like to update.
     * @return
     *      An update builder.
     */
    public Update update(String tableName) {
        Update update = new Update(tableName);
        mQueryParts.add(update);
        return update;
    }

    /**
     * Begin a SQL DELETE statement.
     *
     * EXAMPLE:
     *
     * int numAffected = new SqlBuilder
     *      .delete("table")
     *      .where("id").equalTo("1")
     *      .execute(db);
     *
     * @param tableName
     *      The table you'd like to delete from.
     * @return
     *      A delete builder.
     */
    public Delete delete(String tableName) {
        Delete delete = new Delete(tableName);
        mQueryParts.add(delete);
        return delete;
    }

    private Cursor query(SQLiteDatabase db) {
        if (db == null) {
            throw new IllegalArgumentException("Database instance cannot be null.");
        }
        if (mQueryParts.size() < 2) {
            throw new IllegalStateException("You must have at least a SELECT and FROM clause.");
        }
        if (!(mQueryParts.get(0) instanceof Select)) {
            throw new UnsupportedOperationException("You can only perform queries on 'SELECT' statements.");
        }
        return db.rawQuery(this.toString(), null);
    }

    private int execute(SQLiteDatabase db) {
        if (db == null) {
            throw new IllegalArgumentException("Database instance cannot be null.");
        }
        if (mQueryParts.size() <= 0) {
            throw new IllegalStateException("You have not built a statement.");
        }

        // Grab the where clause if there is one
        String whereClause = null;
        if (mQueryParts.size() > 1 && mQueryParts.get(1) instanceof Where) {
            // Get the where clause with the "WHERE " chopped off from the beginning
            whereClause = ((Where) mQueryParts.get(1)).getTrimmedWhere();
        }

        Object first = mQueryParts.get(0);

        // Updates
        if (first instanceof Update) {
            Update update = (Update) first;
            return db.update(update.mTableName, update.mContentValues, whereClause, null);

            // Deletes
        } else if (first instanceof Delete) {
            Delete delete = (Delete) first;
            return db.delete(delete.mTableName, whereClause, null);

            // Error handling
        } else {
            throw new UnsupportedOperationException("Cannot execute this query.");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Object part : mQueryParts) {
            builder.append(part.toString());
        }
        return builder.toString().trim();
    }

    public class Select {
        private StringBuilder mSelect;

        public Select(String... fields) {
            if (fields == null || fields.length == 0) {
                fields = new String[]{ "*" };
            }
            mSelect = new StringBuilder();
            mSelect.append("SELECT ");
            for (int i = 0; i < fields.length; i++) {
                mSelect.append(fields[i]);
                if (i < fields.length - 1) {
                    mSelect.append(", ");
                }
            }
            mSelect.append(' ');
        }

        public From from(String... tableNames) {
            From from = new From(tableNames);
            mQueryParts.add(from);
            return from;
        }

        @Override
        public String toString() {
            return mSelect.toString();
        }
    }

    public class Update implements Executable, Whereable {
        private String mTableName;
        private ContentValues mContentValues;

        public Update(String tableName) {
            if (tableName == null || tableName.isEmpty()) {
                throw new IllegalArgumentException("You must specify a table name.");
            }
            mTableName = tableName;
            mContentValues = new ContentValues();
        }

        public Update value(String key, Object value) {
            mContentValues.put(key, value == null ? null : value.toString());
            return this;
        }

        public Update withValues(ContentValues values) {
            mContentValues.putAll(values);
            return this;
        }

        @Override
        public Where where() {
            Where where = new Where();
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String clause) {
            Where where = new Where(clause);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String selection, String[] selectionArgs) {
            Where where = new Where(selection, selectionArgs);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("UPDATE ");
            builder.append(mTableName);
            builder.append(" SET ");

            for (String key : mContentValues.keySet()) {
                builder.append(key);
                builder.append(" = ");
                builder.append("'").append(mContentValues.get(key)).append("'");
                builder.append(", ");
            }

            // Replace the dangling ", " with just " "
            builder.replace(builder.length() - 2, builder.length(), " ");
            return builder.toString();
        }

        @Override
        public String build() {
            return SqlBuilder.this.toString();
        }

        @Override
        public int execute(SQLiteDatabase db) {
            return SqlBuilder.this.execute(db);
        }
    }

    public class Delete implements Executable, Whereable {

        public String mTableName;

        public Delete(String tableName) {
            if (tableName == null || tableName.isEmpty()) {
                throw new IllegalArgumentException("You must specify a table name.");
            }
            mTableName = tableName;
        }

        @Override
        public Where where() {
            Where where = new Where();
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String clause) {
            Where where = new Where(clause);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String selection, String[] selectionArgs) {
            Where where = new Where(selection, selectionArgs);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public String toString() {
            return "DELETE FROM " + mTableName + " ";
        }

        @Override
        public int execute(SQLiteDatabase db) {
            return SqlBuilder.this.execute(db);
        }

        @Override
        public String build() {
            return SqlBuilder.this.toString();
        }
    }

    public class From implements Queryable, Whereable {
        private StringBuilder mFrom;

        public From(String... tableNames) {
            if (tableNames == null || tableNames.length == 0) {
                throw new IllegalArgumentException("You must select from at least one table.");
            }
            mFrom = new StringBuilder();
            mFrom.append("FROM ");
            for (int i = 0; i < tableNames.length; i++) {
                mFrom.append(tableNames[i]);
                if (i < tableNames.length - 1) {
                    mFrom.append(", ");
                }
            }
            mFrom.append(' ');
        }

        @Override
        public Where where() {
            Where where = new Where();
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String clause) {
            Where where = new Where(clause);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Where where(String selection, String[] selectionArgs) {
            Where where = new Where(selection, selectionArgs);
            mQueryParts.add(where);
            return where;
        }

        @Override
        public Cursor query(SQLiteDatabase db) {
            return SqlBuilder.this.query(db);
        }

        @Override
        public String build() {
            return SqlBuilder.this.toString();
        }

        @Override
        public String toString() {
            return mFrom.toString();
        }
    }

    public class Where implements Queryable, Executable {

        private StringBuilder mWhere;

        public Where(String selection, String[] selectionArgs) {
            mWhere = new StringBuilder();
            mWhere.append("WHERE ");

            // Build the selection string by replacing the ?'s with the selectionArgs
            for (String selectionArg : selectionArgs) {
                selection = selection.replaceFirst("\\?", "'" + selectionArg + "'");
            }

            mWhere.append(selection);
            mWhere.append(' ');
        }

        public Where(String clause) {
            mWhere = new StringBuilder();
            mWhere.append("WHERE ");
            mWhere.append(clause);
            mWhere.append(' ');
        }

        public Where() {
            mWhere = new StringBuilder();
            mWhere.append("WHERE ");
        }

        /**
         * Equivalent to =. Wraps the value in quotes.
         */
        public Where equalTo(Object value) {
            return equalTo(value, true);
        }
        /**
         * Equivalent to =. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where equalTo(Object value, boolean wrapInQuotes) {
            return operator("=", value, wrapInQuotes);
        }


        /**
         * Equivalent to !=. Wraps the value in quotes.
         */
        public Where notEqualTo(Object value) {
            return notEqualTo(value, true);
        }
        /**
         * Equivalent to !=. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where notEqualTo(Object value, boolean wrapInQuotes) {
            return operator("!=", value, wrapInQuotes);
        }


        /**
         * Equivalent to IN ( 'value1', 'value2', 'value3' ). Wraps each value in quotes.
         */
        public Where in(Object... values) {
            return in(true, values);
        }
        /**
         * Equivalent to IN ( 'value1', 'value2', 'value3' ). Wraps each value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where in(boolean wrapInQuotes, Object... values) {
            return operatorGroup("IN", wrapInQuotes, values);
        }


        /**
         * Equivalent to NOT IN ( 'value1', 'value2', 'value3' ). Wraps each value in quotes.
         */
        public Where notIn(Object... values) {
            return notIn(true, values);
        }
        /**
         * Equivalent to NOT IN ( 'value1', 'value2', 'value3' ). Wraps each value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where notIn(boolean wrapInQuotes, Object... values) {
            return operatorGroup("NOT IN", wrapInQuotes, values);
        }


        /**
         * Equivalent to >. Wraps the value in quotes.
         */
        public Where greaterThan(Object value) {
            return greaterThan(value, true);
        }
        /**
         * Equivalent to >. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where greaterThan(Object value, boolean wrapInQuotes) {
            return operator(">", value, wrapInQuotes);
        }

        /**
         * Equivalent to >=. Wraps the value in quotes.
         */
        public Where atLeast(Object value) {
            return atLeast(value, true);
        }
        /**
         * Equivalent to >=. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where atLeast(Object value, boolean wrapInQuotes) {
            return operator(">=", value, wrapInQuotes);
        }

        /**
         * Equivalent to <. Wraps the value in quotes.
         */
        public Where lessThan(Object value) {
            return lessThan(value, true);
        }
        /**
         * Equivalent to <. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where lessThan(Object value, boolean wrapInQuotes) {
            return operator("<", value, wrapInQuotes);
        }

        /**
         * Equivalent to <=. Wraps the value in quotes
         */
        public Where atMost(Object value) {
            return atMost(value, true);
        }
        /**
         * Equivalent to <=. Wraps the value in quotes.
         * @param wrapInQuotes Whether or not you want to wrap the value in quotes.
         */
        public Where atMost(Object value, boolean wrapInQuotes) {
            return operator("<=", value, wrapInQuotes);
        }

        /**
         * Equivalent to NOT NULL.
         */
        public Where notNull() {
            mWhere.append("NOT NULL ");
            return this;
        }

        /**
         * Equivalent to IS NULL.
         */
        public Where isNull() {
            mWhere.append("IS NULL ");
            return this;
        }

        /**
         * Appends AND without a clause. Created to be used in conjunction with methods like
         * {@link Where#openGroup(String)} and {@link Where#raw(String)}.
         */
        public Where and() {
            mWhere.append("AND ");
            return this;
        }

        public Where and(String clause) {
            mWhere.append("AND ");
            mWhere.append(clause);
            mWhere.append(' ');
            return this;
        }

        /**
         * Appends OR without a clause. Created to be used in conjunction with methods like
         * {@link Where#openGroup(String)} and {@link Where#raw(String)}.
         */
        public Where or() {
            mWhere.append("OR ");
            return this;
        }

        public Where or(String clause) {
            mWhere.append("OR ");
            mWhere.append(clause);
            mWhere.append(' ');
            return this;
        }

        public Where raw(String raw) {
            mWhere.append(raw);
            mWhere.append(' ');
            return this;
        }

        /**
         * Starts a new set of parentheses. You MUST end the group by calling {@link Where#closeGroup()}.
         */
        public Where openGroup(String clause) {
            mWhere.append("( ");
            mWhere.append(clause);
            mWhere.append(' ');
            return this;
        }

        /**
         * Closes the parentheses started by {@link Where#openGroup(String)}. This should ONLY be called if you
         * previously called {@link Where#openGroup(String)}.
         */
        public Where closeGroup() {
            mWhere.append(") ");
            return this;
        }

        private Where operatorGroup(String operator, boolean wrapInQuotes, Object... values) {
            if (values != null && values.length > 0) {
                String singleQuoteString = wrapInQuotes ? "'" : "";

                mWhere.append(operator);
                mWhere.append(" ( ");

                mWhere.append(singleQuoteString);
                mWhere.append(values[0]);
                mWhere.append(singleQuoteString);

                for (int i = 1; i < values.length; i++) {
                    mWhere.append(',').append(' ');
                    mWhere.append(singleQuoteString);
                    mWhere.append(values[i]);
                    mWhere.append(singleQuoteString);
                }

                mWhere.append(" ) ");
            }

            return this;
        }

        private Where operator(String operator, Object value, boolean wrapInQuotes) {
            mWhere.append(operator);
            mWhere.append(' ');
            if (wrapInQuotes) mWhere.append("'");
            mWhere.append(value == null ? null : value.toString());
            if (wrapInQuotes) mWhere.append("'");
            mWhere.append(' ');
            return this;
        }

        private String getTrimmedWhere() {
            return this.toString().substring("WHERE ".length());
        }

        @Override
        public String build() {
            return SqlBuilder.this.toString();
        }

        @Override
        public Cursor query(SQLiteDatabase db) {
            return SqlBuilder.this.query(db);
        }

        @Override
        public int execute(SQLiteDatabase db) {
            return SqlBuilder.this.execute(db);
        }

        @Override
        public String toString() {
            return mWhere.toString();
        }
    }

    private interface Buildable {
        /**
         * Creates a well-formed SQL statement representing the current state of the query. This statement could be run
         * using {@link android.database.sqlite.SQLiteDatabase#rawQuery(String, String[])} or
         * {@link android.database.sqlite.SQLiteDatabase#execSQL(String)}.
         * @return
         *      A well-formed SQL statement representing the current state of the query.
         */
        String build();
    }

    private interface Queryable extends Buildable {
        /**
         * Run the currently-built query on the specified database.
         * @param db
         *      The database to run the query on. Needs to at least be readable.
         * @return
         *      A cursor containing the results of the query.
         */
        Cursor query(SQLiteDatabase db);
    }

    private interface Executable extends Buildable {
        /**
         * Executes the currently-built statement on the specified database.
         * @param db
         *      The database to execute the statement on.
         * @return
         *      The number of affected rows.
         */
        int execute(SQLiteDatabase db);
    }

    private interface Whereable {
        /**
         * Begins a WHERE clause, but with no selection. Intended to be used with {@link Where#openGroup(String)} or
         * {@link Where#raw(String)}.
         * @return
         *      An instance of this class to continue method chaining.
         */
        Where where();

        /**
         * Begins a WHERE clause.
         * @param clause
         *      The left-hand side of your first condition. For example, in the statement "WHERE field1 = 2", the clause
         *      is "field1".
         * @return
         *      An instance of this class to continue method chaining.
         */
        Where where(String clause);

        /**
         * Begins a WHERE clause using an existing selection and selectionArgs that you'd use in the typical
         * Android SQL functions. You could use this method to add in existing conditions, and then easily append others
         * without having to append strings and copy arrays. For documentation on the arguments, see
         * {@link android.database.sqlite.SQLiteDatabase#query(String, String[], String, String[], String, String, String)}.
         * @return
         *      An instance of this class to continue method chaining.
         */
        Where where(String selection, String[] selectionArgs);
    }
}