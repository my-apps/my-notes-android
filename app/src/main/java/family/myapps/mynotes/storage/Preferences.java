package family.myapps.mynotes.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import family.myapps.mynotes.R;

public class Preferences {

    private static final String NAME = "my_notes";
    private final String KEY_CUSTOM_STORAGE;
    private final String KEY_STORAGE_PATH;

    private static Preferences sInstance;

    @NonNull
    private final Context mContext;

    public static void init(@NonNull Context context) {
        sInstance = new Preferences(context);
    }

    public static Preferences getInstance() {
        return sInstance;
    }

    private Preferences(@NonNull Context context) {
        mContext = context;
        KEY_CUSTOM_STORAGE = context.getString(R.string.prefs_key_custom_storage);
        KEY_STORAGE_PATH = context.getString(R.string.prefs_key_storage_location);
    }

    public void onSharedPreferenceChanged(String key) {
        if (KEY_CUSTOM_STORAGE.equals(key) && !getUseCustomStorage()) {
            setStoragePath(getDefaultStoragePath());
        }
    }

    public String getStoragePath() {
        // TODO: What to do if there is no default directory available?
        return getPrefs().getString(KEY_STORAGE_PATH, getDefaultStoragePath());
    }

    public void setStoragePath(String path) {
        getEditor().putString(KEY_STORAGE_PATH, path).apply();
    }

    public boolean getUseCustomStorage() {
        return getPrefs().getBoolean(KEY_CUSTOM_STORAGE, false);
    }

    private String getDefaultStoragePath() {
        return mContext.getExternalFilesDir(null).getAbsolutePath();
    }

    private SharedPreferences getPrefs() {
        return mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }
}
