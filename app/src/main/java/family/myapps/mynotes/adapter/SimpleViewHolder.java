package family.myapps.mynotes.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public abstract class SimpleViewHolder<M> extends RecyclerView.ViewHolder {

    public SimpleViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(@NonNull M model, @Nullable SimpleAdapter.Callback<M> callback);

    public abstract void recycle();

    public interface Factory<M> {
        SimpleViewHolder<M> create(ViewGroup parent);
    }
}
