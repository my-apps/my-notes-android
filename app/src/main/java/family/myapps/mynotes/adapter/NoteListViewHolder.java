package family.myapps.mynotes.adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import family.myapps.mynotes.R;
import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;
import family.myapps.mynotes.util.UiUtil;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteListViewHolder extends SimpleViewHolder<Note> {

    private final TextView mTitle;
    private final TextView mBody;
    private final ViewGroup mTagContainer;
    private final LayoutInflater mInflater;
    private final TagRepository mTagRepository;
    private final LifecycleOwner mLifecycleOwner;

    public NoteListViewHolder(@NonNull View itemView, @NonNull LifecycleOwner lifecycleObserver,
                              @NonNull TagRepository tagRepository) {
        super(itemView);
        mLifecycleOwner = lifecycleObserver;
        mTagRepository = tagRepository;
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mBody = (TextView) itemView.findViewById(R.id.body);
        mTagContainer = (ViewGroup) itemView.findViewById(R.id.tag_container);
        mInflater = LayoutInflater.from(itemView.getContext());
    }

    public void bind(@NonNull final Note note, @Nullable final SimpleAdapter.Callback<Note> callback) {
        mTitle.setText(note.getTitle() != null ? note.getTitle().trim() : "");
        UiUtil.setVisibility(mTitle, mTitle.length() > 0);

        mBody.setText(note.getBody() != null ? note.getBody().trim() : "");
        UiUtil.setVisibility(mBody, mBody.length() > 0);

        itemView.setOnClickListener(view -> {
            if (callback != null) {
                callback.onItemClicked(note);
            }
        });

        mTagRepository.getLiveByNoteId(note.getId()).observe(mLifecycleOwner, this::fillTagContainer);
    }

    public void recycle() {
        itemView.setOnClickListener(null);
    }

    private void fillTagContainer(final List<Tag> tags) {
        // If there are no tags, hide the list
        if (tags.isEmpty()) {
            mTagContainer.setVisibility(View.GONE);
            return;
        }

        // Kick things off with a clean slate
        mTagContainer.setVisibility(View.VISIBLE);
        mTagContainer.removeAllViews();

        // Sort the list so the tags with the shortest names appear first
        Collections.sort(tags, (tag1, tag2) -> tag2.getName().length() - tag1.getName().length());

        // Find the total width available to the view
        int measuredWidth = itemView.getMeasuredWidth();
        if (measuredWidth == 0) {
            // If there's no measured width, it means the view hasn't been layed out yet. A manual force of the measure
            // will not be sufficient. We have to wait for the layout event.
            itemView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    itemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    fillTagContainerImpl(tags, itemView.getMeasuredWidth());
                }
            });
        } else {
            fillTagContainerImpl(tags, measuredWidth);
        }
    }

    private void fillTagContainerImpl(List<Tag> tags, int measuredWidth) {
        // The available width is the total width, less the padding
        final int availableWidth = measuredWidth - (itemView.getPaddingLeft() + itemView.getPaddingRight());

        // Keep adding pills until we make it too wide
        int i = 0;
        while (i < tags.size() && mTagContainer.getMeasuredWidth() < measuredWidth) {
            TextView tagView = inflatePill();
            tagView.setText(tags.get(i).getName());
            mTagContainer.addView(tagView, 0);
            mTagContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            i++;
        }

        // If we overflowed, remove the last one and try to fit a '+n' pill
        if (mTagContainer.getMeasuredWidth() > availableWidth && mTagContainer.getChildCount() > 0) {
            // Remove the last pill
            mTagContainer.removeViewAt(mTagContainer.getChildCount() - 1);
            i--;

            // Add in the number pill
            TextView numberView = inflatePill();
            numberView.setText(itemView.getContext().getString(R.string.n_more_tags, String.valueOf(tags.size() - i)));
            mTagContainer.addView(numberView);
            ((LinearLayout.LayoutParams) numberView.getLayoutParams()).rightMargin = 0;
            numberView.invalidate();

            // If replacing the last tag with a pill caused an overflow, remove the view before the pill and adjust the
            // pill count
            mTagContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            if (mTagContainer.getMeasuredWidth() > availableWidth && mTagContainer.getChildCount() > 1) {
                // Remove the tag before the pill
                mTagContainer.removeViewAt(mTagContainer.getChildCount() - 2);
                i--;

                // Update the text on the pill
                String count = itemView.getContext().getString(R.string.n_more_tags, String.valueOf(tags.size() - i));
                numberView.setText(count);
            } else if (mTagContainer.getMeasuredWidth() > availableWidth) {
                // In the case where not even a pill fits, remove everything
                mTagContainer.removeAllViews();
            }
        }
    }

    private TextView inflatePill() {
        return (TextView) mInflater.inflate(R.layout.item_tag_pill_small, mTagContainer, false);
    }

    public static class Factory implements SimpleViewHolder.Factory<Note> {

        private LifecycleOwner mLifecycleOwner;
        private TagRepository mTagRepository;

        public Factory(@NonNull LifecycleOwner lifecycleObserver, @NonNull TagRepository tagRepository) {
            mLifecycleOwner = lifecycleObserver;
            mTagRepository = tagRepository;
        }

        @Override
        public SimpleViewHolder<Note> create(ViewGroup parent) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
            return new NoteListViewHolder(itemView, mLifecycleOwner, mTagRepository);
        }
    }
}
