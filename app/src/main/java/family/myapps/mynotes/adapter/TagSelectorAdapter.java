package family.myapps.mynotes.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import family.myapps.mynotes.R;
import family.myapps.mynotes.data.NoteRepository;
import family.myapps.mynotes.data.Tag;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagSelectorAdapter extends SimpleAdapter<Tag> {

    private TagSelectorAdapter(long noteId, @NonNull NoteRepository noteRepository) {
        super(new TagSelectorViewHolderFactory(noteId, noteRepository));
    }

    private static class TagSelectorViewHolder extends SimpleViewHolder<Tag> {

        private final long mNoteId;
        private final TextView mName;
        private final CheckBox mCheckbox;
        private final NoteRepository mNoteRepository;

        private TagSelectorViewHolder(@NonNull View itemView, long noteId, @NonNull NoteRepository noteRepository) {
            super(itemView);
            mNoteId = noteId;
            mNoteRepository = noteRepository;
            mName = (TextView) itemView.findViewById(R.id.tag_name);
            mCheckbox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }

        @Override
        public void bind(@NonNull final Tag tag, @Nullable Callback<Tag> callback) {
            mName.setText(tag.getName());
            mCheckbox.setChecked(mNoteRepository.noteContainsTagIdSync(mNoteId, tag.getId()));

            mCheckbox.setOnClickListener(view -> {
                if (mCheckbox.isChecked()) {
                    mNoteRepository.addTagToNote(mNoteId, tag.getId());
                } else {
                    mNoteRepository.removeTagFromNote(mNoteId, tag.getId());
                }
            });

            itemView.setOnClickListener(view -> mCheckbox.performClick());
        }

        @Override
        public void recycle() {
            mCheckbox.setOnClickListener(null);
        }
    }

    private static class TagSelectorViewHolderFactory implements SimpleViewHolder.Factory<Tag> {

        private final long mNoteId;
        private final NoteRepository mNoteRepository;

        private TagSelectorViewHolderFactory(long noteId, @NonNull NoteRepository noteRepository) {
            mNoteId = noteId;
            mNoteRepository = noteRepository;
        }

        @Override
        public SimpleViewHolder<Tag> create(ViewGroup parent) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag_selector, parent, false);
            return new TagSelectorViewHolder(itemView, mNoteId, mNoteRepository);
        }
    }

    public static class Factory {

        private final NoteRepository mNoteRepository;

        public Factory(NoteRepository noteRepository) {
            mNoteRepository = noteRepository;
        }

        public TagSelectorAdapter build(long noteId) {
            return new TagSelectorAdapter(noteId, mNoteRepository);
        }
    }
}
