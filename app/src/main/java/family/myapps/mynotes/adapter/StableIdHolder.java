package family.myapps.mynotes.adapter;

/**
 * Created by greyson on 2/15/17.
 */
public interface StableIdHolder {
    long getItemId();
}
