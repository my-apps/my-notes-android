package family.myapps.mynotes.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import family.myapps.mynotes.R;
import family.myapps.mynotes.data.Tag;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagListViewHolder extends SimpleViewHolder<Tag> {

    private TextView mName;

    public TagListViewHolder(View itemView) {
        super(itemView);
        mName = (TextView) itemView.findViewById(R.id.tag_name);
    }

    public void bind(@NonNull final Tag tag, @Nullable final SimpleAdapter.Callback<Tag> callback) {
        mName.setText(tag.getName());
        itemView.setOnClickListener(view -> {
            if (callback != null) {
                callback.onItemClicked(tag);
            }
        });
    }

    public void recycle() {
        itemView.setOnClickListener(null);
    }

    public static class Factory implements SimpleViewHolder.Factory<Tag> {

        @Override
        public SimpleViewHolder<Tag> create(ViewGroup parent) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false);
            return new TagListViewHolder(itemView);
        }
    }
}
