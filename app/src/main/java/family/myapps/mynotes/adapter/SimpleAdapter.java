package family.myapps.mynotes.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import family.myapps.mynotes.storage.CursorList;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class SimpleAdapter<M extends StableIdHolder> extends RecyclerView.Adapter<SimpleViewHolder<M>> {

    private List<M> mList;
    private SimpleViewHolder.Factory<M> mViewHolderFactory;
    private Callback<M> mCallback;

    public SimpleAdapter(SimpleViewHolder.Factory<M> viewHolderFactory) {
        mList = new ArrayList<>();
        mViewHolderFactory = viewHolderFactory;
        setHasStableIds(true);
    }

    @Override
    public SimpleViewHolder<M> onCreateViewHolder(ViewGroup parent, int viewType) {
        return mViewHolderFactory.create(parent);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder<M> holder, int position) {
        holder.bind(mList.get(position), mCallback);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onViewRecycled(SimpleViewHolder<M> holder) {
        holder.recycle();
    }

    @Override
    public long getItemId(int position) {
        return mList.get(position).getItemId();
    }

    public void setItems(List<M> items) {
        if (mList instanceof CursorList) {
            ((CursorList) mList).close();
        }
        mList = items;
        notifyDataSetChanged();
    }

    public void close() {
        if (mList instanceof CursorList) {
            ((CursorList) mList).close();
            mList = new ArrayList<>();
        }
    }

    public void setCallback(Callback<M> callback) {
        mCallback = callback;
    }

    public interface Callback<M> {
        void onItemClicked(M item);
        void notifyDataSetChanged();
    }
}
