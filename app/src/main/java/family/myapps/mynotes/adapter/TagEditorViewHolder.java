package family.myapps.mynotes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import family.myapps.mynotes.R;
import family.myapps.mynotes.data.Tag;
import family.myapps.mynotes.data.TagRepository;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TagEditorViewHolder extends SimpleViewHolder<Tag> {

    private final View mViewMode;
    private final TextView mName;
    private final View mEditButton;

    private final View mEditMode;
    private final View mDeleteButton;
    private final EditText mNameEditable;
    private final View mDoneButton;
    private final TagRepository mTagRepository;


    TagEditorViewHolder(@NonNull View itemView, @NonNull TagRepository tagRepository) {
        super(itemView);
        mTagRepository = tagRepository;
        mViewMode = itemView.findViewById(R.id.view_mode);
        mName = (TextView) itemView.findViewById(R.id.tag_name);
        mEditButton = itemView.findViewById(R.id.edit_btn);

        mEditMode = itemView.findViewById(R.id.edit_mode);
        mDeleteButton = itemView.findViewById(R.id.delete_btn);
        mNameEditable = (EditText) itemView.findViewById(R.id.tag_name_editable);
        mDoneButton = itemView.findViewById(R.id.done_btn);
    }

    public void bind(@NonNull final Tag tag, @Nullable final SimpleAdapter.Callback<Tag> callback) {
        bind(tag, callback, false);
    }

    private void bind(@NonNull final Tag tag, @Nullable final SimpleAdapter.Callback<Tag> callback, boolean isEditMode) {
        if (!isEditMode) {
            mViewMode.setVisibility(View.VISIBLE);
            mEditMode.setVisibility(View.GONE);

            mName.setText(tag.getName());
            mEditButton.setOnClickListener(view -> bind(tag, callback, true));
        } else {
            mViewMode.setVisibility(View.GONE);
            mEditMode.setVisibility(View.VISIBLE);

            mNameEditable.setText(tag.getName());
            mNameEditable.requestFocus();
            mNameEditable.setOnEditorActionListener((textView, actionId, keyEvent) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDoneButton.performClick();
                }
                return false;
            });
            mDoneButton.setOnClickListener(view -> {
                mTagRepository.updateTagById(tag.getId(), mNameEditable.getText().toString(), success -> {
                    if (!success) {
                        Context context = itemView.getContext();
                        Toast.makeText(context, context.getString(R.string.duplicate_name_error), Toast.LENGTH_SHORT).show();
                    }
                    Tag updatedTag = mTagRepository.getTagByIdSync(tag.getId());
                    bind(updatedTag, callback, false);
                });
            });
            mDeleteButton.setOnClickListener(view -> {
                mTagRepository.deleteTagById(tag.getId(), (Void) -> {
                    if (callback != null) {
                        callback.notifyDataSetChanged();
                    }
                });
            });
        }
    }

    public void recycle() {
        mEditButton.setOnClickListener(null);
        mDoneButton.setOnClickListener(null);
        mDeleteButton.setOnClickListener(null);
    }

    public static class Factory implements SimpleViewHolder.Factory<Tag> {

        private final TagRepository mTagRepository;

        public Factory(TagRepository tagRepository) {
            mTagRepository = tagRepository;
        }

        @Override
        public SimpleViewHolder<Tag> create(ViewGroup parent) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag_editor, parent, false);
            return new TagEditorViewHolder(itemView, mTagRepository);
        }
    }
}
