package family.myapps.mynotes.adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import family.myapps.mynotes.R;
import family.myapps.mynotes.data.Note;
import family.myapps.mynotes.data.TagRepository;
import family.myapps.mynotes.storage.CursorList;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteListAdapter extends RecyclerView.Adapter<SimpleViewHolder> {

    private static final int TYPE_NOTE = 1;
    private static final int TYPE_HEADER = 2;

    private List<Note> mPinnedNotes;
    private List<Note> mUnpinnedNotes;
    private SimpleAdapter.Callback mCallback;

    private final SimpleViewHolder.Factory<Note> mNoteViewHolderFactory;
    private final SimpleViewHolder.Factory<Header> mHeaderViewHolderFactory;

    public NoteListAdapter(@NonNull LifecycleOwner lifecycleOwner, @NonNull TagRepository tagRepository) {
        mPinnedNotes = new ArrayList<>();
        mUnpinnedNotes = new ArrayList<>();
        mNoteViewHolderFactory = new NoteListViewHolder.Factory(lifecycleOwner, tagRepository);
        mHeaderViewHolderFactory = new HeaderViewHolder.Factory();
        setHasStableIds(true);
    }

    public void setPinnedNotes(List<Note> pinnedNotes) {
        mPinnedNotes = pinnedNotes;
        notifyDataSetChanged();
    }

    public void setUnpinnedNotes(List<Note> unpinnedNotes) {
        mUnpinnedNotes = unpinnedNotes;
        notifyDataSetChanged();
    }

    public void setNotes(List<Note> notes) {
        mPinnedNotes = new ArrayList<>();
        mUnpinnedNotes = notes;
        notifyDataSetChanged();
    }

    public void close() {
        if (mPinnedNotes instanceof CursorList) {
            ((CursorList) mPinnedNotes).close();
            mPinnedNotes = new ArrayList<>();
        }
        if (mUnpinnedNotes instanceof CursorList) {
            ((CursorList) mUnpinnedNotes).close();
            mUnpinnedNotes = new ArrayList<>();
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return mHeaderViewHolderFactory.create(parent);
        }
        return mNoteViewHolderFactory.create(parent);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bind(getModelAtPosition(position), mCallback);
    }

    @Override
    public void onViewRecycled(SimpleViewHolder holder) {
        holder.recycle();
    }

    @Override
    public int getItemViewType(int position) {
        if (getModelAtPosition(position) instanceof Header) {
            return TYPE_HEADER;
        }
        return TYPE_NOTE;
    }

    @Override
    public int getItemCount() {
        if (mPinnedNotes.size() > 0) {
            return mPinnedNotes.size() + mUnpinnedNotes.size() + 2;
        }
        return mUnpinnedNotes.size();
    }

    @Override
    public long getItemId(int position) {
        return getModelAtPosition(position).getItemId();
    }

    public void setCallback(SimpleAdapter.Callback callback) {
        mCallback = callback;
    }

    private StableIdHolder getModelAtPosition(int position) {
        if (mPinnedNotes.size() > 0) {
            if (position == 0) {
                return new Header(Header.ID_PINNED_HEADER);
            } else if (position <= mPinnedNotes.size()) {
                return mPinnedNotes.get(position - 1);
            } else if (position == mPinnedNotes.size() + 1) {
                return new Header(Header.ID_UNPINNED_HEADER);
            } else {
                return mUnpinnedNotes.get(position - mPinnedNotes.size() - 2);
            }
        } else{
            return mUnpinnedNotes.get(position);
        }
    }

    private static class Header implements StableIdHolder {

        private static final int ID_PINNED_HEADER = -1;
        private static final int ID_UNPINNED_HEADER = -2;

        private long mId;

        public Header(long id) {
            mId = id;
        }

        public String getTitle(Context context) {
            if (mId == ID_PINNED_HEADER) {
                return context.getString(R.string.pinned_title);
            }
            return context.getString(R.string.unpinned_title);
        }

        @Override
        public long getItemId() {
            return mId;
        }
    }

    private static class HeaderViewHolder extends SimpleViewHolder<Header> {

        private TextView mTitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.title);
        }

        @Override
        public void bind(@NonNull Header model, @Nullable SimpleAdapter.Callback<Header> callback) {
            mTitle.setText(model.getTitle(itemView.getContext()));

            // Make the item span all columns
            StaggeredGridLayoutManager.LayoutParams layoutParams =
                    (StaggeredGridLayoutManager.LayoutParams) itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }

        @Override
        public void recycle() {
        }

        private static class Factory implements SimpleViewHolder.Factory<Header> {

            @Override
            public SimpleViewHolder<Header> create(ViewGroup parent) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_note_list_header,
                        parent,
                        false);
                return new HeaderViewHolder(itemView);
            }
        }
    }
}
