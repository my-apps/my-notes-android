package family.myapps.mynotes;

import android.app.Application;

import family.myapps.mynotes.di.AppComponent;
import family.myapps.mynotes.di.AppModule;
import family.myapps.mynotes.di.DaggerAppComponent;
import family.myapps.mynotes.storage.Preferences;
import family.myapps.mynotes.data.FileStorage;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class MyNotesApplication extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Preferences.init(this);
        sAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
