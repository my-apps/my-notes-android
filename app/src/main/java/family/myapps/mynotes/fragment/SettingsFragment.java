package family.myapps.mynotes.fragment;

import android.Manifest;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.greysonparrelli.permiso.Permiso;

import net.rdrei.android.dirchooser.DirectoryChooserConfig;
import net.rdrei.android.dirchooser.DirectoryChooserFragment;

import family.myapps.mynotes.R;
import family.myapps.mynotes.storage.Preferences;

public class SettingsFragment extends PreferenceFragment implements
        DirectoryChooserFragment.OnFragmentInteractionListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG_FILE_PICKER = "file_picker";

    private Preference mStorageLocationPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Find preferences and register listeners
        findPreference(getString(R.string.prefs_key_custom_storage)).setOnPreferenceClickListener(preference -> onCustomStorageClicked());
        mStorageLocationPref = findPreference(getString(R.string.prefs_key_storage_location));
        mStorageLocationPref.setOnPreferenceClickListener(preference -> onStorageLocationClicked());
        updateStorageLocationSummary();

        // Re-attach listener if file picker is active
        DirectoryChooserFragment filePickerFragment =
                (DirectoryChooserFragment) getFragmentManager().findFragmentByTag(TAG_FILE_PICKER);
        if (filePickerFragment != null) {
            filePickerFragment.setDirectoryChooserListener(this);
            filePickerFragment.setTargetFragment(this, 0);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preferences.getInstance().onSharedPreferenceChanged(key);
        updateStorageLocationSummary();
    }

    @Override
    public void onSelectDirectory(@NonNull String newPath) {
        String originalPath = Preferences.getInstance().getStoragePath();
        if (!newPath.equals(originalPath)) {
            Preferences.getInstance().setStoragePath(newPath);
//            FileStorage.getInstance().moveNotesToNewLocation(originalPath, newPath);
        }

        updateStorageLocationSummary();
        dismissFilePicker();
    }

    @Override
    public void onCancelChooser() {
        dismissFilePicker();
    }

    private boolean onCustomStorageClicked() {
        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
            @Override
            public void onPermissionResult(Permiso.ResultSet resultSet) {
                if (!resultSet.areAllPermissionsGranted()) {
                    // TODO: String res
                    Toast.makeText(SettingsFragment.this.getActivity(), "You must allow access to external storage.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return true;
    }

    private boolean onStorageLocationClicked() {
        // Initialize Builder
        final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                .newDirectoryName("my-notes")
                .allowNewDirectoryNameModification(true)
                .allowReadOnlyDirectory(false)
                .build();
        DirectoryChooserFragment fragment = DirectoryChooserFragment.newInstance(config);
        fragment.setDirectoryChooserListener(this);
        fragment.setTargetFragment(this, 0);
        fragment.show(getFragmentManager(), TAG_FILE_PICKER);
        return true;
    }

    private void updateStorageLocationSummary() {
        String path = Preferences.getInstance().getStoragePath();
        String summary = String.format(getString(R.string.settings_storage_location_summary), path);
        mStorageLocationPref.setSummary(summary);
    }

    private void dismissFilePicker() {
        DirectoryChooserFragment fragment = (DirectoryChooserFragment) getFragmentManager().findFragmentByTag(TAG_FILE_PICKER);
        if (fragment != null) {
            fragment.dismiss();
        }
    }
}
