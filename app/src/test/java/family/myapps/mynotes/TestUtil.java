package family.myapps.mynotes;

import java.io.File;
import java.net.URL;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class TestUtil {
    public static File getFileFromPath(Object obj, String fileName) {
        ClassLoader classLoader = obj.getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        return new File(resource.getPath());
    }
}
