package family.myapps.mynotes.util;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class CircularBufferTest {

    private CircularBuffer<Integer> mUnderTest;
    private final static int SIZE = 4;

    @Before
    public void setup() {
        mUnderTest = new CircularBuffer<>(SIZE);
    }


    @Test
    public void add_untilFull() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        mUnderTest.add(4);

        assertEquals(1, (int) mUnderTest.get(0));
        assertEquals(2, (int) mUnderTest.get(1));
        assertEquals(3, (int) mUnderTest.get(2));
        assertEquals(4, (int) mUnderTest.get(3));
    }

    @Test
    public void add_overflowOne() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        mUnderTest.add(4);
        mUnderTest.add(5);

        assertEquals(2, (int) mUnderTest.get(0));
        assertEquals(3, (int) mUnderTest.get(1));
        assertEquals(4, (int) mUnderTest.get(2));
        assertEquals(5, (int) mUnderTest.get(3));
    }

    @Test
    public void add_overflowTwoFullTimesPlusOne() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        mUnderTest.add(4);
        mUnderTest.add(5);
        mUnderTest.add(6);
        mUnderTest.add(7);
        mUnderTest.add(8);
        mUnderTest.add(9);

        assertEquals(6, (int) mUnderTest.get(0));
        assertEquals(7, (int) mUnderTest.get(1));
        assertEquals(8, (int) mUnderTest.get(2));
        assertEquals(9, (int) mUnderTest.get(3));
    }

    @Test
    public void addAndGet_untilFull() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        Integer popped = mUnderTest.addAndGet(4);

        assertEquals(1, (int) mUnderTest.get(0));
        assertEquals(2, (int) mUnderTest.get(1));
        assertEquals(3, (int) mUnderTest.get(2));
        assertEquals(4, (int) mUnderTest.get(3));
        assertEquals(null, popped);
    }

    @Test
    public void addAndGet_overflowOne() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        mUnderTest.add(4);
        Integer popped = mUnderTest.addAndGet(5);

        assertEquals(2, (int) mUnderTest.get(0));
        assertEquals(3, (int) mUnderTest.get(1));
        assertEquals(4, (int) mUnderTest.get(2));
        assertEquals(5, (int) mUnderTest.get(3));
        assertEquals(1, (int) popped);
    }

    @Test
    public void addAndGet_overflowTwoFullTimesPlusOne() {
        mUnderTest.add(1);
        mUnderTest.add(2);
        mUnderTest.add(3);
        mUnderTest.add(4);
        mUnderTest.add(5);
        mUnderTest.add(6);
        mUnderTest.add(7);
        mUnderTest.add(8);
        Integer popped = mUnderTest.addAndGet(9);

        assertEquals(6, (int) mUnderTest.get(0));
        assertEquals(7, (int) mUnderTest.get(1));
        assertEquals(8, (int) mUnderTest.get(2));
        assertEquals(9, (int) mUnderTest.get(3));
        assertEquals(5, (int) popped);
    }

    @Test
    public void size_init() {
        assertEquals(0, mUnderTest.size());
    }

    @Test
    public void size_oneItem() {
        mUnderTest.add(1);
        assertEquals(1, mUnderTest.size());
    }

    @Test
    public void size_full() {
        for (int i = 0; i < SIZE; i++) {
            mUnderTest.add(i);
        }

        assertEquals(SIZE, mUnderTest.size());
    }

    @Test
    public void size_overflowOne() {
        for (int i = 0; i < SIZE; i++) {
            mUnderTest.add(i);
        }
        mUnderTest.add(SIZE);
        assertEquals(SIZE, mUnderTest.size());
    }

    @Test
    public void toList_empty() {
        assertEquals(0, mUnderTest.toList().size());
    }

    @Test
    public void toList_full() {
        for (int i = 0; i < SIZE; i++) {
            mUnderTest.add(i);
        }

        List<Integer> list = mUnderTest.toList();
        for (int i = 0; i < SIZE; i++) {
            assertEquals(i, (int) list.get(i));
        }
    }

    @Test
    public void toList_overflowOne() {
        for (int i = 0; i < SIZE; i++) {
            mUnderTest.add(i);
        }
        mUnderTest.add(SIZE);

        List<Integer> list = mUnderTest.toList();
        assertEquals(1, (int) list.get(0));
        assertEquals(2, (int) list.get(1));
        assertEquals(3, (int) list.get(2));
        assertEquals(4, (int) list.get(3));
    }
}
