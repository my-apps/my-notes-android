package family.myapps.mynotes.data.parser;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import family.myapps.mynotes.TestUtil;
import family.myapps.mynotes.data.NoteWithTags;

import static org.junit.Assert.assertEquals;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class NoteParserV2Test {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    private NoteParserV2 mSubject;

    @Before
    public void setup() {
        mSubject = new NoteParserV2();
    }

    @Test
    public void fillFromString_titleBody() {
        String fileContents = getFileContents("note-files/title-body.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(0, noteWithTags.note.getCreatedTimestampMs());
        assertEquals(0, noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titleBodyCreated() throws ParseException {
        String fileContents = getFileContents("note-files/title-body-created.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(DATE_FORMAT.parse("1991-12-30 13:14:15").getTime(), noteWithTags.note.getCreatedTimestampMs());
        assertEquals(0, noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titleBodyCreatedUpdated() throws ParseException {
        String fileContents = getFileContents("note-files/title-body-created-updated.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(DATE_FORMAT.parse("1991-12-25 12:13:14").getTime(), noteWithTags.note.getCreatedTimestampMs());
        assertEquals(DATE_FORMAT.parse("1991-12-30 13:14:15").getTime(), noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titleBodyNoNewLine() {
        String fileContents = getFileContents("note-files/title-body-no-newline.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(0, noteWithTags.note.getCreatedTimestampMs());
        assertEquals(0, noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titleBodyTag() {
        String fileContents = getFileContents("note-files/title-body-tag.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(1, noteWithTags.tags.size());
        assertEquals("tag", noteWithTags.tags.get(0).getName());
    }

    @Test
    public void fillFromString_titleBodyTagsMultiword() {
        String fileContents = getFileContents("note-files/title-body-tags-multiword.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(2, noteWithTags.tags.size());
        assertEquals("tag 1", noteWithTags.tags.get(0).getName());
        assertEquals("tag 2", noteWithTags.tags.get(1).getName());
    }

    @Test
    public void fillFromString_titleBodyTagsTwo() {
        String fileContents = getFileContents("note-files/title-body-tags-two.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(2, noteWithTags.tags.size());
        assertEquals("tag1", noteWithTags.tags.get(0).getName());
        assertEquals("tag2", noteWithTags.tags.get(1).getName());
    }

    @Test
    public void fillFromString_titleBodyUpdated() throws ParseException {
        String fileContents = getFileContents("note-files/title-body-updated.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("Body", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(0, noteWithTags.note.getCreatedTimestampMs());
        assertEquals(DATE_FORMAT.parse("1991-12-30 13:14:15").getTime(), noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titleOnly() {
        String fileContents = getFileContents("note-files/title-only.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("", noteWithTags.note.getBody());
        assertEquals(false, noteWithTags.note.isPinned());
        assertEquals(0, noteWithTags.note.getCreatedTimestampMs());
        assertEquals(0, noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    @Test
    public void fillFromString_titlePinned() {
        String fileContents = getFileContents("note-files/title-pinned.md");
        NoteWithTags noteWithTags = mSubject.parse(fileContents);

        assertEquals("Title", noteWithTags.note.getTitle());
        assertEquals("", noteWithTags.note.getBody());
        assertEquals(true, noteWithTags.note.isPinned());
        assertEquals(0, noteWithTags.note.getCreatedTimestampMs());
        assertEquals(0, noteWithTags.note.getUpdatedTimestampMs());
        assertEquals(0, noteWithTags.tags.size());
    }

    private String getFileContents(String path) {
        File file = TestUtil.getFileFromPath(this, path);
        StringWriter output = new StringWriter();
        try {
            IOUtils.copy(new FileReader(file), output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output.toString();
    }
}
