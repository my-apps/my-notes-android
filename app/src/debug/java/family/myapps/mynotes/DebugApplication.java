package family.myapps.mynotes;


import com.facebook.stetho.Stetho;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class DebugApplication extends MyNotesApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
