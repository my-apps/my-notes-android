# My Notes

An Android note-taking app where you own your data.

## Release Signing
To sign a release, the build system expects that there is a file called
"keystore.properties" in the root of your project with the following contents:

```
keyAlias=<keyAlias>
keyPassword=<keyPassword>
storeFile=<path to .jks>
storePassword=<storePassword>
```
